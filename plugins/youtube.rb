module Jekyll
  class Youtube < Liquid::Tag

    def initialize(name, id, tokens)
      super
      @id = id
    end

    def render(context)
      %(<div class="embed-video-container">
        <div class="embed-video-container-dummy1">
        <div class="embed-video-container-dummy2">
        <iframe src="https://www.youtube.com/embed/#{@id}"></iframe>
        </div></div></div>)
    end
  end
end

Liquid::Template.register_tag('youtube', Jekyll::Youtube)
