# Title: Fancyimage tag for Jekyll
# Authors: Devin Weaver (photos_tag.rb), Brian M. Clapper (img_popup.rb), Patrick Paul (this gist)
# Description: Takes full size image, automagically creates thumbnail at specified size, +fancybox
#
# Adapted from:
#   http://tritarget.org/blog/2012/05/07/integrating-photos-into-octopress-using-fancybox-and-plugin/
#   (photos_tag.rb) https://gist.github.com/2631877
#   (img_popup.rb) https://github.com/bmc/octopress-plugins/
#
# Syntax {% photo filename [tumbnail] [title] %}
# Syntax {% photos filename [filename] [filename] [...] %}
# If the filename has no path in it (no slashes)
# then it will prefix the `_config.yml` setting `photos_prefix` to the path.
# This allows using a CDN if desired.
#
# To make FancyBox work well with OctoPress You need to include the style fix.
# In your `source/_include/custom/head.html` add the following:
#
#     {% fancyboxstylefix %}
#
# Markup:
#   {% fancyimage right /path/to/image.png 200x100 This is a cool title! %}
#
#   {% fancyalbum 100x100! %}
#   /uploads/image.png: Image caption here
#   /another/upload.png
#   {% endfancyalbum %}
#
# Output:
#   <a href="/path/to/image.png" class="fancybox" title="This is a cool title!">
#   <img class="right" src="/path/to/image200x100.png" alt="This is a cool title!" /></a>
#
# Todo:
#   Figure out how to generate thumbnail images directly into /public/ folder 
#       (instead of to /source/ -> rake generate)
#
# Installation:
#   Forthcoming. Look at the plugins I adapted off of for now.
#   I also aliases 'rake bake' to 'rake generate, rake generate, rake deploy'
#       because the thumbnails only generate -> /public/ the second time around.

require 'digest/md5'
require 'mini_magick'

def generate_thumbnail(context, filename, dimension)
  # Determine the full path to the source image
  site = context.registers[:site]
  source = Pathname.new(site.source).expand_path
  image_path = source + filename.sub(%r{^/}, '')
  basename = File.basename(image_path)
  
  image_extension = File.extname(basename)
  safe_dims = (@dimensions).gsub(/!/,"_abs") # replace ! with _abs since filenames can't contain it
  thumb_filename = basename.split(".")[0] + "_" + safe_dims + image_extension
  rel_path = "images/thumbs/"
  thumb_path = source + (rel_path+thumb_filename).sub(%r{^/}, '')
  
  # Resize and write the thumbnail image, if it doesn't exist yet
  if not Dir.exists?(source + rel_path)
    Dir.mkdir(source + rel_path)
  end
  if not File.exists?(thumb_path) or File.ctime(thumb_path) < File.ctime(image_path)
    puts("generating thumbnail for #{thumb_path}")
    thumb = MiniMagick::Image.open(image_path)
    thumb.resize(@dimensions)
    thumb.write("#{thumb_path}")
    site.static_files << Jekyll::StaticFile.new(thumb, source, rel_path, thumb_filename)
  end
  return rel_path + thumb_filename
end

module Jekyll

  class FancyboxStylePatch < Liquid::Tag
    def render(context)
      return <<-eof
<!-- Fix FancyBox style for OctoPress -->
<style type="text/css">
  .fancybox-wrap { position: fixed !important; }
  .fancybox-opened {
    -webkit-border-radius: 4px !important;
       -moz-border-radius: 4px !important;
            border-radius: 4px !important;
  }
  .fancybox-close, .fancybox-prev span, .fancybox-next span {
    background-color: transparent !important;
    border: 0 !important;
  }
</style>
      eof
    end
  end

  class FancyImageTag < Liquid::Tag
    def initialize(tag_name, markup, tokens)
      args = markup.strip.split(/\s+/, 4)
      
      raise "Usage: fancyimage img-class filepath 123x456 [title] (input: fancyimage #{markup})" unless [3, 4].include? args.length
    
      @class = args[0]
      if @class
        @class = @class.gsub(/,/, ' ')
      end
      @filename = args[1]
      @dimensions = args[2]
      @title = args[3]
      super
    end

    def render(context)
      thumb_path = generate_thumbnail(context, @filename, @dimension)
      
      if @filename
        img = "<a href=\"/#{@filename}\" class=\"fancybox\" title=\"#{@title}\"><img class=\"noborder\" src=\"\/#{thumb_path}\" alt=\"#{@title}\" /></a>"
        if @title and not @class.match(/\bnocaption\b/)
          img << "<figcaption>#{@title}</figcaption>"
        end
        "<figure class=\"fancyimage #{@class}\">#{img}</figure>"
      else
        "Error processing input, expected syntax: {% fancyimage img-class filename 123x456 [title] %}"
      end
    end
  end

  class FancyAlbumTag < Liquid::Block
    #Sample syntax:
    #{% fancyalbum %}
    #photo1.jpg
    #photo2.jpg[thumb2.jpg] # REMOVED
    #photo3.jpg[thumb3.jpg]: my title 3 # REMOVED
    #photo4.jpg: my title 4
    #{% fancyalbum %}
    
    def initialize(tag_name, markup, tokens)
      args = markup.strip.split(/\s+/, 4)
      
      raise "Usage: fancyalbum 123x456 (input: fancyalbum #{markup})" unless [1].include? args.length
    
      @dimensions = args[0]
      super
    end

    def render(context)
      lines = "#{@nodelist}" # ["\nimages/midasfull.png: my title 1\nuploads/Fahrenheit451.jpg: my title 2\nimages/midasfull.png: my title 3\n"]
      md5 = Digest::MD5.hexdigest(lines)
      lines = lines.gsub(/^\[/,'')
      lines = lines.gsub(/^\]/,'')
      lines = lines.gsub(/^\"/,'')
      lines = lines.split('\n')
      list = ""

      lines.each do |line|
        if line.length < 3 # this is how we purge empty rows within the tag block
           next
        end
        if /(?<filename>[^\[\]:]+)?(?::(?<title>.*))?/ =~ line          
          html_title = ""
          if title != nil
            title = title.strip()
            html_title = "<figcaption>#{title}</figcaption>"
          end
          # Determine the full path to the source image
          filename = filename.gsub(/\n/,'')
          thumb_path = generate_thumbnail(context, filename, @dimensions)
          html  = "<figure class=\"fancyimage\">"
          html << "  <a href=\"/#{filename}\" class=\"fancybox\" rel=\"gallery-#{md5}\" title=\"#{title}\">"
          html << "    <img class=\"noborder\" src=\"/#{thumb_path}\" alt=\"#{title}\" />"
          html << "  </a>"
          html << "  #{html_title}"
          html << "</figure>"
          list << html
        end
      end
      "<div class=\"fancygallery\">\n#{list}</div>"
    end
  end

end

Liquid::Template.register_tag('fancyimage', Jekyll::FancyImageTag)
Liquid::Template.register_tag('fancyalbum', Jekyll::FancyAlbumTag)
Liquid::Template.register_tag('fancyboxstylefix', Jekyll::FancyboxStylePatch)
