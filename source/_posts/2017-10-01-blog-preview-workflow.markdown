---
layout: post
title: "Octopress Workflow: Online Preview"
date: 2017-10-18 20:00
comments: true
author: [chrissi^]
categories: [english, howto, meta]
---

As a hackerspace a blog is another great opportunity to use all that nerd tools that we tend to talk about.
For example this blog is hosted using [Octopress]. And, of course, content is managed using Git and [gitli].

[Octopress]: http://octopress.org/
[gitli]: https://gitli.stratum0.org/

Since [rohieb] has just added another way of writing and previewing 
blog posts, I want to update our blog's documentation.

[rohieb]: https://twitter.com/daniel_bohrer

Working Offline
-------------------

This is the oldest and most bare metal way of writing and previewing a new post. This basically means installing
Ruby and Octopress on your machine. Valodim has written some  [documentation] in one of our first blog posts.

[documentation]: https://stratum0.org/blog/posts/2013/09/08/octopress-workflow/


Collaboration
----------------

If you want online collaboration, you may want to give our Etherpad integration a try.

Ever noticed the hourglass tool in a Stratum 0 [pad]? If you write your post in a pad and click that icon, [bogomir] will render a blog containing just your article.
I have prepared a [demo pad] with some source from an old post.

[pad]: https://pad.stratum0.org
[bogomir]: https://stratum0.org/wiki/Bogomir
[demo pad]: https://pad.stratum0.org/p/9Sz5DanggK

Another nice thing about this workflow is that you don't need a local Ruby setup. But: The big disadvantage is that you are not able to test the images you have embedded into your post (since there is no way to upload them).


All-yes online preview
---------------------------

Git has the advantage of *branches*, i.e. related versions of code running parallel. For an overview of working with Git branches, refer to the chapter [Git Branching] in the Pro Git book.

[Git Branching]: https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell

Usually the `master` branch is used to build the actual blog at <https://stratum0.org/blog/>.

This workflow introduces a new branch into the [blog Git]: The `preview` branch is used to build a preview at <https://sandbox.stratum0.org/blog/>.

[blog Git]: https://gitli.stratum0.org/stratum0/stratum0-blog/branches

This is done using GitLab CI. The actual magic is configured in [`.gitlab-ci.yml`][yml].
In this case you can use all features of Octopress – including embedded images.

[yml]: https://gitli.stratum0.org/stratum0/stratum0-blog/blob/master/.gitlab-ci.yml

Another advantage of this workflow if the possibility to review new posts. 
If you think you finished your work you can clean up your Git history and file a [merge request].
It's then up to someone else to have a look at your work, maybe correct some last typos and then 
merge it into `master` and thus publish your post.

[merge request]: https://gitli.stratum0.org/stratum0/stratum0-blog/merge_requests
