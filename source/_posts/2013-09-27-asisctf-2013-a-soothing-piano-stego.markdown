---
layout: post
title: "Asisctf 2013: A Soothing Piano"
date: 2013-09-27 16:42
comments: true
categories: [english,ctf,writeup,asisCTF13]
---

## A Soothing Piano

Challenge description:
	For your listening pleasure.

This forensic challenge gave us a mp3 file containing soothing piano music.

<!-- more -->

The left and right audio channels were nearly the same, except that in the left channel a voice was reading the flag for us.
In audacity, you can recover the audio as follows: split the audio channels into two tracks, invert the right channel, set it to be a left channel and finally mix the two tracks together again.

There is also a much more simple solution though:
Just click on effect -> plugins -> voice remover, click ok and wait a few seconds.
The voice remover tool will extract differences in the left and right audio channels for you.

{% img left /images/asis2013/piano_flag.png %}
Now you can see the voice at the 3 minute mark, which will read the flag for you (the image shows the voice after amplification).

Since we couldn't understand everything, we bruteforced the last few characters using the hash of the flag that was given on the submission page.

