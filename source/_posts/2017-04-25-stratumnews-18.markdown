---
layout: post
title: "Stratumnews Vol. 18"
author: Stratumnews Team
date: 2017-04-25 01:00
comments: true
categories: [german,stratumnews]
---

Hallo Entitäten,

an dieser Stelle wieder eine zwei(±n)wöchentliche Zusammenfassung for all things space.
<!--more-->

HackenOpenAir
-------------
Unser erster [hoa-blogpost][hb] auf events.ccc.de wurde veröffentlicht, habt ihr schon eure Tickets? Wenn nein, [jetzt klicken][jk]!

[hb]: https://events.ccc.de/2017/04/04/hacken-open-air/
[jk]: https://tickets.hackenopenair.de/

Neuer Beamer auf dem Holodeck
-----------------------------
Von shoragan wurde ein neuer Beamer zur Verfügung gestellt, jetzt mit nativem hdmi und 720p. Die Ansteuerung erfolgt jetzt über den Verstärker im Subraum, es sollte noch ein längeres HDMI-Kabel beschafft werden.

Hängematte im Subraum
---------------------
Wer sich erinnert: Dort hing vor einiger Zeit mal eine Hängematte, Chrissi^ hat diesen Zustand jetzt [wiederhergestellt][hm]. Vorsicht: gefährlich gemütlich.

[hm]: https://twitter.com/larsan/status/848234374118158337

Sushiabend
----------
Am Sa. 22.04. trafen wir uns zum gemeinsamen Sushibauen. Etwa 23 Entitäten nahmen teil und wurden sehr sehr [satt][su].

[su]: https://twitter.com/Stratum0/status/855871510694776832

Wurstworkshop
------------
Wer frische, selbstgemachte Bratwurst zum Grillen haben möchte, ist hier genau richtig aufgehoben. Am 13.05. ist es wieder soweit.
Mehr Infos im [Wiki][wi], Anmeldung bis 08.05. im [Pad][pa].

[wi]: https://stratum0.org/wiki/Wurstworkshop
[pa]: https://pad.stratum0.org/p/bratworscht

SPAC3
-----
Die Arbeitsgruppe SPAC3 hat sich getroffen und sich vom Hausmeister des Schimmelhofs potentielle Räumlichkeiten zeigen lassen.
Zu diesen Räumlichkeiten haben wir mittlerweile auch einen Grundriss, die gesamte Situation wird im [Arbeitsgruppenpad][ap] genauer beschrieben.
Die Fotos der Besichtigung liegen auf dem NAS.
Aktueller Stand der Diskussion ist: Herumspinnen, welche Räume man wie nutzen wollen würde.
Es ist geplant, demnächst ein Treffen dazu einzuberufen.

[ap]: https://pad.stratum0.org/p/arbeitsgruppe\_space\_3

Shenzhen-Portal
---------------
Bisher hattest du die Wahl: Jetzt sofort teuer bei Conrad kaufen, etwas günstiger in Deutschland bestellen und ein paar Tage warten, oder lange auf die Bestellung aus China warten, dafür weniger bezahlen.
Das ist jetzt vorbei! Im Space gibt es nun das [Shenzhen-Portal][sp]: Kleine Bauteile von wenigen Cent bis einigen Euro sind hier [verfügbar][vf]. Zu günstigen Konditionen und direkt vor Ort!

[sp]: https://stratum0.org/wiki/Shenzhen-Portal
[vf]: https://matrix.stratum0.org/\_matrix/media/v1/download/stratum0.org/qdNkqUgkgZgUSfRvvCFFuioP

Freifunk-Reboot
---------------
Freifunk Braunschweig ist dabei, einen sauberen Neustart vorzubereiten. Dazu fand am 23.04. ein mehrstündiger Freifunk-Reboot-Workshop statt, der von 13 Entitäten besucht wurde. Es wurde viel diskutiert und Dinge zusammen getragen, die man besser machen könnte, und wie man sie besser machen könnte.
Als erste Frucht dieses Workshops wird am Mi., 03.05. - im Anschluss an das reguläre Freifunktreffen - das Backend vom neuen ffbsNG genauer ausgearbeitet.

Misc
----
* Kasa und larsan haben den Pipettierroboter und die alte Spülmaschine weggebracht.
* Der Switch im Frickelraum hat jetzt neue leise Lüfter, ein weiterer Satz Lüfter ist eingelagert.
* Auf Herbert ist nun auch 5D Embroidery installiert und ist damit vermutlich nun das Gerät der Wahl zur Stickvorbereitung.

Termine
-------
* Di, 25.04. 16:30: Schreibrunde
* Di, 25.04. 18:00: Malkränzchen
* Di, 25.04. 19:00 - 23:00: Anime Referat
* Mi, 26.04. 19:00: Arbeiten mit Holz
* Mi, 26.04. 19:00: Freifunk-Treffen
* Do, 27.04. 19:00 - 23:00: Captain's Log
* Di, 02.05. 18:00: Malkränzchen
* Di, 02.05. 18:00: Coptertreffen, Allgemeiner Infoaustausch
* Mi, 03.05. 19:00: Freifunk - ffbsNG Infrastrukturtreffen
* Mi, 03.05. 19:00: Freifunk-Treffen
* Mi, 03.05. 19:00: Vegan Academy
* Do, 04.05. 18:30: Digital Courage Braunschweig, offenes Ortsgruppentreffen
* Mo, 08.05. 19:00: CoderDojo Vorbereitungsbasteln
* Di, 09.05. 18:00: Malkränzchen
* Di, 09.05. 19:00 - 23:00: Anime Referat
* Mi, 10.05. 19:00: Freifunk-Treffen
* Do, 11.05. 19:00 - 23:00: Captain's Log
* Sa, 13.05. 12:00: Wurstworkshop - Wurst selbst herstellen
* So, 14.05. 19:00: Vorträge
* Di, 16.05. 18:00: Coptertreffen, Allgemeiner Infoaustausch
* Mi, 17.05. 19:00: Vegan Academy
* Do, 18.05. 18:30: Digital Courage Braunschweig, offenes Ortsgruppentreffen
* Sa, 20.05. 14:00: CoderDojo
* Do, 25.05. bis So, 28.05.: *GPN17*, Karlsruhe
* Sa, 03.06. 20:00: KeyMatic Batteriewechselparty
* Mi, 21.06. 01:00 bis Mi, 21.06. 07:00: Sommersonnenwendenwanderung 2017
* Fr, 14.07. 16:00 bis Sa, 15.07. 16:00:  #enno2017

Willst du diesen Newsletter mitgestalten? Dann schicke deinen kurzen Beitrag an <kontakt@stratum0.org>.

Happy Hacking!

