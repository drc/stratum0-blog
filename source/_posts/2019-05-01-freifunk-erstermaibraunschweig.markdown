---
layout: post
title: "Freifunk Braunschweig beim Internationalen Fest zum 1. Mai in Braunschweig"
date: 2019-05-01 20:00
comments: true
author: chrissi^
categories: [german, freifunk]
---

Der Freifunk Braunschweig war dieses Jahr mit mit seinem [Event-WLAN]
beim [Internationalen Fest] des DGB im Rahmen der Proteste zum 1. Mai
im Bürgerpark dabei.

Bei bestem Wetter haben wir mit den vielen Besuchern für Solidarität und
für (digitale) Bürgerrechte demonstriert.

Infostand
------------

Wir waren mit einem Stand dabei und haben über Freifunk in
Braunschweig informiert. 
Zum Einen haben wir gezeigt, wo in der Region bereits
Zugangspunkte und Meshes vorhanden sind. 
Zum Anderen haben wir darüber informiert, welche Möglichkeiten es gibt
bei Freifunk mitzumachen und somit Teil der Initiative zu werden.

{% fancyalbum 300x300 %}
images/posts/2019-05-01-freifunk-erstermai/stand.jpg: Der Freifunk Braunschweig Stand
images/posts/2019-05-01-freifunk-erstermai/stand1.jpg: Unser Netzwerkkern: Offen zur Schau gestellt
{% endfancyalbum %} 

Event-WLAN
-------------

Zusätzlich haben wir unser Eventnetz im Bürgerpark aufgebaut: 
Wir haben weit über 200m Kabeln und elf Accesspoints aufgebaut um im
gesamten Veranstaltungsbereich "Freifunk"-WLAN zur Verfügung zu stellen.

Dabei haben wir alle Seiten unserer Eventnetz-Technik zum Einsatz gebracht:
Wo möglich haben wir unseren outdoorfähigen Accesspoints an Pavillons und
Bäumen aufgehangen.
Brücken zwischen unterschiedlichen Spannungsversorgungen wurden dabei per
Glasfaser geschlagen.
Bei der längsten Strecke wurde anstatt auf Kabel auf Funk gesetzt und
somit die Verbindung über den See im Bürgerpark mit einer Richtfunkstrecke
realisiert.

{% fancyalbum 300x300 %}
images/posts/2019-05-01-freifunk-erstermai/callisto.jpg: Große Accesspoints lassen sich bestens mit Spanngurten befestigen.
images/posts/2019-05-01-freifunk-erstermai/richtfunk.jpg: Wo Spanngurte zu kurz sind löst Band das Problem...
images/posts/2019-05-01-freifunk-erstermai/thebe.jpg: Kleine Accesspoints lassen sich super an den Pavillons anderer Stände verteilen.
images/posts/2019-05-01-freifunk-erstermai/buehne.jpg: Wo kein kabelgebundener Uplink möglich war haben wir Unifi AP Mesh eingesetzt.
images/posts/2019-05-01-freifunk-erstermai/box.jpg: Weitere aktive Technik haben wir in unseren Transportboxen zusammengefasst.
{% endfancyalbum %} 

Über den Tag waren insgesamt 1305 Clients mit unserem Netz verbunden und
haben knapp 21GB Daten im WLAN geladen.
Zur Spitzenzeit waren knapp 400 Clients gleichzeitig mit unserem Netz
verbunden.

{% fancyalbum 300x300 %}
images/posts/2019-05-01-freifunk-erstermai/graph.png: Clients und Traffic
{% endfancyalbum %} 

Der Traffic der Clients wurde mit zwei gleichzeitigen LTE Uplinks zu einem
unserer Freifunk-Gateways gebracht und von dort via [Freifunk Rheinland]
ins Internet gebracht.

WLAN auf deiner Veranstaltung?
-------------------------------

Du planst ebenfalls eine gemeinnützige und/oder kostenlose Veranstaltung
für alle und möchtest deinen Gästen freies und unzensiertes WLAN anbieten?
Setze dich mit uns in [Verbindung]. 
Wir helfen gern.

[Event-WLAN]: https://freifunk-bs.de/event.html
[Internationalen Fest]: https://erstermaibraunschweig.de/wp-content/uploads/2019/04/1.-Mai-Plakat-Braunschweig-2019.pdf
[Freifunk Rheinland]: https://freifunk-rheinland.net/
[Verbindung]: https://freifunk-bs.de/kontakt.html
