---
layout: post
title: "MakerVsVirus in Braunschweig"
date: 2020-04-07 20:00
comments: true
author: Marco, Pecca, larsan
categories: [german, stratum0, covid19]
---
Seit dem [letzten Update vor knapp einer Woche][s0vsv] hat sich einiges getan:

[s0vsv]: https://stratum0.org/blog/posts/2020/04/07/makervsvirus/

## Bedarf & Produktion

Insgesamt haben wir einen Bedarf von 4590 Schirmen von 79 Organisationen, Praxen und Kliniken gemeldet bekommen. 
Hiervon konnten wir bereits (Stand 07.04. 18:42 Uhr) 2249 Schirme ausliefern und damit den Bedarf von über 65 Praxen, Pflegeheimen u.ä. vollständig bedienen, weitere 522 Schirme warten gepackt auf die Auslieferung, oder sind zur Zeit schon unterwegs.
Allein 2000 der 4590 Schirme verteilen sich auf drei Großabnehmer (Krisenstäbe und Kliniken), die wir in mehreren kleinen Chargen beliefern, um kleine wie große Abnehmer gleichermaßen möglichst sofort bedienen zu können.

Hergestellt werden die Schirme mittlerweile von insgesamt 89 verschiedenen Personen und Organisationen (darunter das Protohaus, das DLR und die TU BS) auf insgesamt 136 Druckern. Die Produktionskapazität hat sich somit nochmals deutlich auf ca. 700 St / Tag erhöht.

Durch die hohen Produktionszahlen ist auch die Materialbeschaffung und -logistik mittlerweile deutlich gewachsen. Sowohl die Bestellung von Filament wie auch der Folie für das eigentliche Schild erfordern einigen Einsatz.
Nachdem unser erster Folieneinkauf nur 20 Zuschnitte aus 0,5 mm PET-Folie für unsereren Lasercutter umfasste, mussten wir zeitnah nachlegen. Der nächste Einkauf, der dank der vielen Spenden möglich war, bestand aus mehreren Dutzend Quadratmetern 0,8 mm PETG-Folie.
Doch auch der Vorrat ging schnell zuneige und die Verfügbarkeit von Folie bei Händlern wurde merklich knapper.
An dieser Stelle ein großes Dankeschön an die Firma Wipak Walsrode, die uns an dieser Stelle als Hersteller unterstützt hat. Und uns dabei nicht nur ein paar Quadratmeter, sondern gleich eine ganze Rolle 0,5 mm PET-Folie gesponsert hat!

Filament befindet sich im Zulauf von mehreren Lieferanten und wird permanent im Space angeliefert. Über 200 kg Filament und 1000 m Lochgummiband sind bereits verarbeitet worden.

{% fancyalbum 300x300 %}
images/posts/2020-04-07-makervsvirus/fertige_schirme.jpg: Fertige Gesichtsschirme, noch mit Abziehfolie
images/posts/2020-04-07-makervsvirus/folienrolle.jpg: 1,02 x 850 m PET-Folie
images/posts/2020-04-07-makervsvirus/knopflochgumminator3000.jpg: Wir haben das Zuschneiden des Gummibands etwas optimiert.
{% endfancyalbum %}


## Organisation & Fahrdienst

Sowohl die Auslieferungen fertiger Masken wie auch die Abholung von gedruckten Teilen und die Anlieferung von Filament übernimmt im Großraum Braunschweig bei Bedarf mittlerweile ein Fahrdienst aus 5 Personen.

Die Abstimmung des Gesamtteams erfolgt wie gehabt je nach Bedarf über Slack, sowie einer täglichen Telefonkonferenz. Für die Koordination der Besetzung des Stratum 0 als zentrale Anlaufstelle dient eine Threema-Gruppe.

Gerüchteweise gibt es teilweise schon Livetracking vom Fahrdienst.


## Behelfs-Mundschutz-Masken

Uns erreichten zusammen mit einigen Anfragen nach Schirmen die Frage nach Mundschutzmasken. Diese können wir zwar nicht bereitstellen, aber auch dafür gibt es bereits eine Initiative in Braunschweig: 
In einem Zusammenschluss von der [Bürgerstiftung Braunschweig](https://www.buergerstiftungbraunschweig.de/), dem [Sandkasten](https://www.sandkasten.tu-braunschweig.de/sandkasten) der TU und dem Stoffgeschäft [SchickLiesel](https://www.schickliesel.net/) werden diese produziert. Mehr Infos findet man auf der [Projektseite des Sandkastens](https://www.sandkasten.tu-braunschweig.de/projekte/mundschutz-naehen-fuer-medizinische-einrichtungen).

Auch hierfür wird noch fleißige Unterstützung gesucht.


## Spenden & Finanzen

Der Moneypool hat inzwischen einen Gesamtstand von 9900 Euro erreicht, weitere 935 Euro sind auf dem Konto des Stratum 0 eingegangen.
7130 Euro wurden hiervon bereits für Filament, Folie, Gummibänder ausgegeben, knapp 3500 Euro stehen demnach noch zur Verfügung.

Weitere Spenden bitte auf das Konto des Stratum 0 unter Bankverbindung mit dem Verwendungszweck "MakerVsVirus".

Da der Stratum 0 e.V. gemeinnützig ist, können diese Spenden steuerlich geltend gemacht werden. Für Spenden bis 200 EUR reicht i.d.R. der Kontoauszug als Beleg gegenüber dem Finanzamt aus. Bei größeren Spenden setzt euch bitte unter vorstand@stratum0.org mit uns in Verbindung. Mehr Informationen dazu findet ihr unter [Spenden](https://stratum0.org/wiki/Spenden) im Wiki.

Der aktuelle Spendenstand ist [hier](https://data.stratum0.org/finanz/) unter "Aktuelle Zweckbindungen" zu finden. 


## Pressespiegel 
* Heise: [Spendenaufruf: MakerVsVirus bauen Face Shields für Kliniken](https://www.heise.de/make/meldung/Coronavirus-Maker-helfen-mit-Teilen-aus-dem-3D-Drucker-4685194.html)
* Wolfsburger Nachrichten: [Initiative stellt Gesichtsschutz mit 3D-Druckern her (Paywall)](https://www.wolfsburger-nachrichten.de/wolfsburg/article228814673/Initiative-stellt-Gesichtsschutz-mit-3D-Druckern-her.html) (so auch im Gifhorner Teil veröffentlicht.)
* Braunschweiger Zeitung: [Braunschweigs Feuerwehr Chef: Wir erwarten mehr Corona-Patienten](https://www.braunschweiger-zeitung.de/braunschweig/article228867215/Braunschweigs-Feuerwehr-Chef-Wir-erwarten-mehr-Corona-Patienten.html)
