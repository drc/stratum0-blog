---
layout: post
title: "How-To: Stainless Steel Etching (2 Updates)"
date: 2014-03-06 14:44
comments: true
author: larsan
tldr: 
categories: [english,howto,DIY]
---

{% fancyimage left images/posts/2014-03-06-how-to-stainless-steel-etching/00-plotter.jpg 300x300 A Cogi (PCUT) CT-630 %}
A few days ago one of our members gifted us a cutting plotter. 

While we were still in the process of figuring out, how it works I stumbled across [a reddit post][redditpost] by **/u/itsgus**.

Since I happened to have a couple of stainless steel cups lying around an idea was born to combine these three.

<!-- more -->

First we needed to decide what we wanted to etch onto the cups. Though this was not really up for decision:
Apart from several different aperture logos, one of our motifs was a cake (not a lie).
There are plenty of suitable svg files available via image search of which we wickedly downloaded one.
(Though, during writing of this howto, I ran across this piece of [condensed awesomeness][awesomeness])

{% fancyimage center images/posts/2014-03-06-how-to-stainless-steel-etching/01-inkscape1.png 500x500 Before editing %}


Image processing was done with inkscape and the inkcut extension, I might dig into that in another post since the workflow is rather plotter-specific.

{% fancyimage center images/posts/2014-03-06-how-to-stainless-steel-etching/02-inkscape2.png 500x500 After editing %}

After appyling some inkscape magic and raging about some inkcut bugs (still don't know what caused it) we now hold in our hands a piece of adhesive foil on substrate, that has the stencil cut into it.

 "Prepare the black matter!" 

{% fancyalbum 300x300 %}
images/posts/2014-03-06-how-to-stainless-steel-etching/03-blackmatter.jpg: Prepare the black matter!
images/posts/2014-03-06-how-to-stainless-steel-etching/04-firstcut.jpg: Sweet sharp cut
images/posts/2014-03-06-how-to-stainless-steel-etching/05-secondcut.jpg: You can use scissorcs for this - or lasers
{% endfancyalbum %}

Once it's cut, you can remove the actual motif to get the stencil.

{% fancyimage center images/posts/2014-03-06-how-to-stainless-steel-etching/06-weeding.jpg 500x500 After weeding %}

Doing this is actually called "weeding", heh.

{% fancyimage center images/posts/2014-03-06-how-to-stainless-steel-etching/10-transfer1.jpg 500x500 Transfer Paper on Stencil %}

Now we add a layer of transfer paper (or foil) onto the mask.

{% fancyimage center images/posts/2014-03-06-how-to-stainless-steel-etching/11-cleaner.jpg 500x500 Stainless Steel Cream Cleaner %}

It might be prudent to ensure that the surface is as clean as possible to ensure best results, we used stainless steel cream cleanser for the cups.

{% fancyimage center images/posts/2014-03-06-how-to-stainless-steel-etching/12-transfer2.jpg 500x500 Removing the substrate %}

When you remove the substrate, make sure that all parts of the stencil stay attached to the transfer paper, the advantage of this method to have stencils without bridges is in this regard a small drawback.

Apply the mask to the cup and remove the transfer paper.

  
{% fancyalbum 300x300 %}
images/posts/2014-03-06-how-to-stainless-steel-etching/13-transfer3.jpg: Apply cake to cup
images/posts/2014-03-06-how-to-stainless-steel-etching/14-transfer4.jpg: Cupcake!
images/posts/2014-03-06-how-to-stainless-steel-etching/15-transfer5.jpg: Not a lie!
{% endfancyalbum %}


Now comes the fun part that itsgus described: etching.
First we need an acid (vinegar does the job) and an electrolyte (salt) you really don't need much.
As for the ratio: We just added salt and stirred until the vinegar was saturated.



You will need a direct current for this to work, itsgus used a 9V block battery, we had a DC power supply at hand. More important is the polarity! Attach the positive pole to the cup and the negative pole to your advanced etching contraption. 
A Q-tip works fine. We built ourselves a little tool for this purpose.

{% fancyalbum 300x300 %}
images/posts/2014-03-06-how-to-stainless-steel-etching/09-dcpowersupply.jpg: This is CURRENTly our solution for this.
images/posts/2014-03-06-how-to-stainless-steel-etching/07-contraption.jpg: Contraption
images/posts/2014-03-06-how-to-stainless-steel-etching/16-etching1.jpg: No need for a lot of pressure
{% endfancyalbum %}

Dip the tip into the solution and stroke it over the mask.

{% fancyimage center images/posts/2014-03-06-how-to-stainless-steel-etching/17-etching2.jpg 500x500 You can tell the difference %}

You don't need to apply any pressure since all the work is done chemically, not mechanically.

{% fancyimage center images/posts/2014-03-06-how-to-stainless-steel-etching/18-water.jpg 500x500 Wash it off. %}

When you etched all areas more or less evenly, you can wash off the residues and then remove the stencil.

{% fancyimage center images/posts/2014-03-06-how-to-stainless-steel-etching/19-finished.jpg 500x500 Finished! %}
{% fancyimage center images/posts/2014-03-06-how-to-stainless-steel-etching/20-company.jpg 500x500 In good company. %}

Who needs laser engraving anyway? The finest structure we've etched so far was around 0.7 mm, but we guess it's possible to go down to 0.2 mm.

You can find the images of this post [in this flickr set][flickrset].


[redditpost]: http://www.reddit.com/r/DIY/comments/1zekac/how_to_add_permanent_volume_markings_to_a_kettle/ "How to add peermanent volume markings to a kettle"
[awesomeness]: http://zeptozephyr.deviantart.com/art/Vectored-Portal-Icons-207347804 "Vectored Portal Icons"
[flickrset]: http://www.flickr.com/photos/larspace/sets/72157641948682015/ "How-to: Stainless Steel Etching"

*Update (by rohieb):* [I tried it on brass][brass], works reasonably well (as
long as you don’t get the direction of the adhesive wrong… ;-))

[brass]: http://imgur.com/a/hagYe

*2nd Update (also by rohieb):* After buying a bunch of new (steel) belt buckles,
I tried an Extended Advanced Etching Contraption:

{% youtube zXGG2v3C-Hg %}

Although the belt buckle was wrapped in duct tape, the vinegar-salt solution
leaked through, which resulted in slight etching on the inside on the
buckle. If this is a problem for you, you should be really careful when
wrapping it, and maybe try submerging it in clean water first ;-) But in my
case, it’s on the inside and nobody will ever notice.
