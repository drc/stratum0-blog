---
layout: post
title: "Stratumnews Vol. 22"
author: Stratumnews-Team
date: 2017-08-27 23:30
comments: true
categories: [german,stratumnews]
---

Hallo Entitäten,

an dieser Stelle wieder eine zwei(±n)wöchentliche Zusammenfassung for all things space:


Vermisst und/oder gefunden
--------------------------

Die Diskokugel aus dem Bad ist verschwunden, sachdienliche Hinweise werden dankend entgegengenommen.

Im [Lost&Found-Pad][lf] hat sich nicht viel getan und im Space liegen nachwievor Unmengen an Klamotten rum. Es haben zwei ein/zwei Entitäten Namen von potentiellen Besitzern ins Pad geschrieben, aber wurden die auch informiert, oder sollte das auf magische Weise von selbst geschehen?

[lf]: https://pad.stratum0.org/p/stratum0_lost_and_found


Labdoo
------

Der Space ist jetzt als [Labdoo-Hub] registriert. Operativ bedeutet das hauptsächlich Laptops bekommen, $Dinge installieren, wieder wegschicken. Für einen guten Zweck und so.
Die Treffen des Labdoo-Hubs werden erst einmal nach Bedarf geplant. Wer interessiert ist sollte die [Mailingliste] abonieren.
Wir bekommen wahrscheinlich bald die ersten Laptops von der [Hilfe für Liberia]. \o/

[Labdoo-Hub]: https://www.labdoo.org/hub?h=45540
[Mailingliste]: https://lists.stratum0.org/mailman/listinfo/labdoo-hub
[Hilfe für Liberia]: https://www.liberia-projekte.de/


Marktplatz der Kreativen
------------------------

Am 01. September sind wir auf dem Marktplatz der Kreativen auf dem Altstadtmarkt vertreten. Am Montag, 28.08. um 18:00 findet dazu ein Vorbereitungstreffen statt.


34c3
----

Wir haben wieder [Voucher] für Tickets bekommen, sie wurden schon fleißig rumgereicht. Wer Interesse hat, trage sich bitte ins Pad ein. Sobald ihr einen Voucher habt, klickt und bezahlt bitte zügig ein Ticket, sodass wir schnell durch die Warteliste kommen.

[Voucher]: https://pad.stratum0.org/p/34c3vouchers


Strom
-----

Wir haben vor, den Anbieter zu wechseln, von Naturstrom zu EWS-Schönau. Unter der illusorischen Annahme, dass jetzt nicht mehr Strom verbraucht werden würde, könnten wir damit dreistellig pro Jahr sparen.


Metro und Selgros
-----------------

Der Verein hat jetzt sowohl für Metro als auch für Selgros Kundenkarten. Alle, die sich nach dem initialen Aufruf gemeldet haben, haben eine Karte bekommen (oder können sie sich abholen). Bei Fragen bitte bei larsan melden. Die Karten sind zwar entitätengebunden, aber zumindest bei Metro übertragbar. Bei Selgros kann man mit einfacher Einzeiler-Vollmacht ebenfalls einkaufen. Kundennummer darauf genügt, ggf. noch die Karte mitnehmen.


Serverdienste
-------------

Auf <https://paste.stratum0.org> läuft jetzt ein [PrivateBin], ein aktiv weiterentwickelter Fork von ZeroBin. Zusätzlich zu Bugfixes gibt es neue Features, so können Pastes jetzt zusätzlich mit einem Passwort versehen werden und Dateien mit bis zu 2 MiB als Anhang zu einem Paste hochgeladen werden. Mit ZeroBin erstellte Pastes sind weiterhin über die gleiche URL erreichbar, da unser PrivateBin komplett abwärtskompatibel ist.

Außerdem ist nun eine eigene [Dudle]-Instanz zur Terminabstimmung unter <https://dudle.stratum0.org> verfügbar. Umfragen werden für 90 Tage aufbewahrt und dannach wieder weggeräumt.

[PrivateBin]: https://privatebin.info
[Dudle]: https://github.com/kellerben/dudle


Internet
--------

Manche haben vielleicht vom Internet-Upgrade-Versuch mitbekommen, aktueller Stand ist:
Zwar kann unser DSLAM mittlerweile DSL100, aber es sind keine Vectoring-Ports mehr frei.


Termine
-------

  * Mo, 28.08. 18:00: Vorbereitungstreffen Marktplatz der Kreativen 2017
  * Mo, 28.08. 19:00: HOA: Orgatreffen
  * Di, 29.08. 18:00: Malkränzchen
  * Mi, 30.08. 16:00: Schreibrunde
  * Mi, 30.08. 17:30 - 20:00: 3D-Druck(er) Sprechstunde
  * Mi, 30.08. 19:00: Freifunk-Treffen
  * Do, 31.08. 18:00 - 21:30: BS LUG
  * Do, 31.08. 19:00 - 23:00: Captain's Log
  * Fr, 01.09. 13:00 - 18:00: Marktplatz der Kreativen 2017, Altstadtmarkt
  * Mo, 04.09. 18:00: Malkränzchen
  * Mo, 04.09. 19:00: HOA: Orgatreffen
  * Di, 05.09. 18:00: Coptertreffen, Kopter Meeting im Stratum0
  * Mi, 06.09. 16:00: Schreibrunde
  * Mi, 06.09. 19:00: Freifunk-Treffen
  * Mi, 06.09. 20:00: Vegan Academy
  * Do, 07.09. 18:00 - 21:30: BS LUG
  * Do, 07.09. bis So, 10.09.: HackenOpenAir
  * Do, 07.09. 18:30: Digital Courage Braunschweig, offenes Ortsgruppentreffen
  * Do, 07.09. 19:00 - 23:00: Anime Referat
  * Mo, 11.09. 18:00: Malkränzchen
  * Mi, 13.09. 16:00: Schreibrunde
  * Mi, 13.09. 17:30 - 20:00: 3D-Druck(er) Sprechstunde
  * Mi, 13.09. 19:00: Freifunk-Treffen
  * Do, 14.09. 18:00 - 21:30: BS LUG
  * Do, 14.09. 19:00: Vorträge
  * Do, 14.09. 19:00 - 23:00: Captain's Log
  * Fr, 15.09. bis So, 17.09.: LHC VI: Large Hackerspace Convention, Shackspace
  * Di, 19.09. 18:00: Coptertreffen, Kopter Meeting im Stratum0
  * Mi, 20.09. 20:00: Vegan Academy
  * Do, 21.09. 18:30: Digital Courage Braunschweig, offenes Ortsgruppentreffen
  * Do, 21.09. 19:00 - 23:00: Anime Referat
  * Sa, 23.09. 14:00: CoderDojo
  * So, 24.09. 14:00: Mitgliederversammlung 2017-09-24

Willst du diesen Newsletter mitgestalten? Dann schicke deinen kurzen Beitrag an <kontakt@stratum0.org>.

Happy Hacking!
