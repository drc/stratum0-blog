---
layout: post
title: "Asisctf 2013: Inaccessible"
date: 2013-09-27 17:32
comments: true
categories: [english,ctf,writeup,asisCTF13]
---

## Inaccessible

Challenge description:

	Find the flag.

The challenge gave us a file asisfor.iso that was a modified damn small linux.

<!-- more -->

{% img center /images/asis2013/inaccessible_boot.png %}

You could login with an empty password and find a file /root/secret.txt with a bunch of base64 encoded data.
If you extracted the contents of the iso, you could find that the file was written at boot time from different boot-up scripts.
After recovering the bas64 string and decoding it, it resulted in a corrupted .gif image.

{% img center /images/asis2013/corrupted.gif 500 %}

The corrupted gif was enough to recognize most of the characters of the flag, so that we could brute force the rest from the hash given on the submission page.

