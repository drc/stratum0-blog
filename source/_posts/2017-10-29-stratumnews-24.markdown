---
layout: post
title: "Stratumnews Vol. 24"
author: Stratumnews-Team
date: 2017-10-28 18:40
comments: true
categories: [german,stratumnews]
---

Hallo Entitäten,

an dieser Stelle wieder eine zwei(±n)wöchentliche Zusammenfassung for all things space:

<!-- more -->

Vorträge
--------

Wir haben wieder Vortragsaufzeichnungen \o/
Noch nicht technisch perfekt, aber besser als zuvor und weitere Verbesserungen sind geplant.
Die Aufzeichnungen sind auf [YouTube] zu finden.

[YouTube]: https://www.youtube.com/user/stratum0


Internet im Space
-----------------

Es gab ein [Pad], ein bisschen Diskussion, ein paar Probleme mit Webshops,
schlussendlich haben wir jetzt ein PC-Engines [APU2C4]-Board gekauft.
Das ist mittlerweile eingerichtet und hat den bisherigen Router ersetzt.
Weitere Informationen und Zukunftsplanungen sind im Wiki unter [strapu] zu
finden.

[Pad]: https://pad.stratum0.org/p/internet_im_space
[APU2C4]: https://www.pcengines.ch/apu2.htm
[strapu]: https://stratum0.org/wiki/Strapu


CoderDojo
---------

Das CoderDojo hat Besuch von der [Braunschweiger Zeitung] bekommen.
Möglicherweise brauchen wir zum nächsten Termin ein paar mehr Mentoren,
Interesse? Das nächste CoderDojo findet am 18.11. statt.

[Braunschweiger Zeitung]: http://www.braunschweiger-zeitung.de/braunschweig/article212325211/Hier-tuefteln-Programmierer-von-morgen.html


Misc
----

Unser Gitlab funktioniert nun auch über IPv6… \*hust\*

Der Infodisplaypi ist dank funktionierendem libcec wieder in der Lage das Infodisplay abzuschalten.


lost+found
----------

TP-Link TL-WDR4300 WLAN-AP mit schwarzem Gehäuse und drei Antennen. Wurde zuletzt vor dem HOA auf dem Holodeck gesehen, wo er hinter der Abdeckung eingebaut werden sollte.
Hat ggf. noch ein geplottetes ["twi-fi"-Logo] auf dem Gehäuse.

Die Werkstattwand vermisst eine blaue Kombizange, einen 13er- und einen 17er Ringmaulschlüssel.

Die Löt- und Elektronikecke vermisst ein Schraubendreherset in einer grünen Plastikbox.

["twi-fi"-Logo]: https://imgur.com/1INBHw8


Termine
-------

*  Mo, 30.10. 18:00: Malkränzchen, offener Kreativabend
*  Di, 31.10. 17:30 - 20:00: 3D-Druck(er) Sprechstunde
*  Di, 31.10. 18:00: Coptertreffen, Kopter Meeting im Stratum0
*  Mi, 01.11. 19:00: Freifunk-Treffen
*  Do, 02.11. 18:30: Digital Courage Braunschweig, offenes Ortsgruppentreffen
*  Mo, 06.11. 18:00: Malkränzchen, offener Kreativabend
*  Di, 07.11. 19:00: Arbeitstreffen Geschäftsführung
*  Mi, 08.11. 19:00: Freifunk-Treffen
*  Do, 09.11. 19:00 - 23:00: Captain's Log
*  Sa, 11.11. 13:37 bis So, 12.11. 18:00: #Coldney17
*  So, 12.11. 15:00:  #Coldney17-Aufräumtreff
*  Mo, 13.11. 18:00: Malkränzchen, offener Kreativabend
*  Di, 14.11. 17:30 - 20:00: 3D-Druck(er) Sprechstunde
*  Di, 14.11. 18:00: Coptertreffen, Kopter Meeting im Stratum0
*  Di, 14.11. 19:00: Vorträge
*  Mi, 15.11. 19:00: Freifunk-Treffen
*  Do, 16.11. 18:30: Digital Courage Braunschweig, offenes Ortsgruppentreffen
*  Sa, 18.11. 14:00: CoderDojo, Anmeldung via eventbrite, siehe [aktueller Blogpost]
*  Do, 23.11. 19:00 - 23:00: Captain's Log
*  Fr, 24.11. bis So, 26.11.: Glühweinprogrammiernacht (GlüPN), Karlsruhe
*  Mi, 27.12. bis Sa, 30.12.: 34c3, Leipzig

[aktueller Blogpost]: /blog/posts/2017/10/23/coderdojo-braunschweig/

Willst du diesen Newsletter mitgestalten? Dann schicke deinen kurzen Beitrag an <kontakt@stratum0.org>.

Happy Hacking!

