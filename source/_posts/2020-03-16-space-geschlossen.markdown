---
layout: post
title: "Vorübergehende Schließung des Spaces"
date: 2020-03-16 21:00
comments: true
author: chrissi^
categories: [german, stratum0, covid19]
---

Moin,

Die niedersächsische Regierung hat heute die weitgehende 
[Einschränkung des öffentlichen Lebens beschlossen](https://www.ms.niedersachsen.de/startseite/service_kontakt/presseinformationen/massnahmen-im-kampf-gegen-covid-19-land-untersagt-alle-offentlichen-veranstaltungen-schliessung-aller-freizeit-und-kultureinrichtungen-und-teile-des-einzelhandels-186324.html).

**Update 18. April:** Die Maßnahmen wurden [bis vorerst 6. Mai verlängert](https://www.ms.niedersachsen.de/startseite/aktuelles/presseinformationen/land-stellt-neue-verordnung-vor-erste-lockerungen-fur-einzelhandel-und-schule-187567.html).

Darin heißt es:

 >  Wir haben die Gesundheitsbehörden angewiesen, alle öffentlichen Veranstaltungen sowie private Versammlungen in
 >  Niedersachsen zu untersagen. Auch sämtliche Kultur- und Freizeiteinrichtungen sind ab Dienstag zu schließen.
 >  Das gleiche gilt für die Teile des Einzelhandels, die nicht für den täglichen Bedarf erforderlich sind.


Ob eine einzelne Person im Space anwesend sein darf oder nicht, ist wohl nicht eindeutig geregelt.
Wir möchten den Wortlaut jedoch zur Vermeidung von weiterer Ausbreitung der Pandemie und auch zur 
Vermeidung von Streitigkeiten mit den Behörden nicht ausreizen.
Wir werden den Space daher – bis auf Weiteres – auch für Mitglieder schließen.
Dies gilt ab Dienstag, 17. März 2020 06:00 Uhr bis voraussichtlich <strike>18. April</strike> **6. Mai**.

Eure privaten Gegenstände könnt ihr natürlich wie bisher aus dem Space abholen, 
ebenso könnt ihr entsprechend der bisherigen Regelung Gegenstände aus dem Space ausleihen.
Eure Projekte sollen schließlich nicht unter der aktuellen Lage leiden.
Bitte haltet euch nicht mit anderen Personen im Space auf, und verweilt nur für 
die Zeit des Abholens in den Räumlichkeiten, um auch anderen Personen diese 
Möglichkeit zu bieten.

Achtet darauf, keine verderblichen Gegenstände im Space zu lagern, das betrifft 
insbesondere den Kühlschrank und die Mülleimer.
Es wird sich niemand um die Entsorgung kümmern können.

Bitte bestellt bis auf Weiteres keine Post in den Space, da sie niemand annehmen kann.

Über Änderungen werden wir euch über die üblichen Wege informieren.
Bei Fragen wendet euch an <vorstand@stratum0.org>.

Bitte bleibt alle gesund, damit wir das Space-Leben gemeinsam nach Ausklingen 
der Pandemie umso mehr genießen können!

Der Vorstand
