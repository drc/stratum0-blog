---
layout: post
title: "Stratumnews Vol. 26"
author: Stratumnews-Team
date: 2018-02-25 22:10
comments: true
categories: [german,stratumnews]
---

Hallo Entitäten,

an dieser Stelle wieder eine zwei(±n)wöchentliche Zusammenfassung for all things
space:

<!-- more -->


Mitgliederversammlung
---------------------

Am 14.01.2018 fand die 9. Mitgliederversammlung des Stratum 0 e.V. statt, es
wurde unter anderem ein neuer Vorstand gewählt. Er besteht aus: larsan (1V),
rohieb (2V), emantor (Schatzmeister), blinry, reneger, chamaeleon.


SpacekisteV4
------------

Es hat ein Komplettupdate der Spacekiste gegeben, die neue [SpacekisteV4] ist
nun wieder leiser und um einiges performanter als die SpacekisteV3.

Die Maschine wird zukünftig mit einem voctomix auch im Rahmen von
Vortragsaufzeichnungen genutzt werden. In Richtung Vortragsaufzeichnung
interessierte tauschen sich derzeit in einem [matrix room] aus.

[SpacekisteV4]: https://stratum0.org/wiki/Spacekiste#v4
[room]: https://chat.stratum0.org/#/room/#voctomix:datenverein.de


Spacebauabend 03
----------------

Es fand wieder ein Spacebauabend statt:

* Der neue Briefkasten wurde gedoomt (ist wieder ohne schlüssel bedienbar), ist
  aber noch sehr schwergängig.
* Audio- und HDMI-Kabel im Chillraum haben jetzt eine Halterung
* Platine vom Vakuumierer ist getauscht worden, LED sollte jetzt wieder gehen
* Der Lulzbot hat ein Gehäuse bekommen, ggf. lassen sich jetzt die ABS-Reste
  besser verdrucken.
* Mehr Licht in der Wekstatt: drei zusätzliche LED-Röhren
* Zwischen Spüle und Spülmaschine hängt jetzt ein Handtuchhalter für weniger
  Geschirrhandtücher auf der Arbeitsplatte.
* Holodecklicht sollte jetzt nach Netzausfall ohne Neustart des Pis wieder gehen
* Offene Kanten in den Kanten der Chillraumdecke wurden mit Acryl zugeschmiert
* Das Overflow-Klo im Flur hat eine rudimentäre Reinigung bekommen
* Der zweite Tisch in der Lötecke hat jetzt eine zusätzliche Beleuchtung
* Am folgenden Tag wurden noch in der Werkstatt diverse Regale angeschraubt
* Weiteres Todo, was derzeit in Bearbeitung ist: Gehäuse für den
  Komponententester

Weitere offene Todos sind wieder unter der Leinwand auf dem Brüstungskanal
ausgelegt. Bei Fragen bitte an larsan wenden.


Vorträge
--------

Im Januar und Februar gab es wieder einige Vorträge, von den folgenden Vorträgen
gibt es [Aufzeichnungen]:

* Januar
    * AberDerBart: Show & Tell: tldr & thefuck
    * Emantor: How to Vorstand (Was macht der da eigentlich?)
    * Drahflow: sfhttpd
* Februar
    * Emantor: RISC-V - User-Level ISA, Privilege-Level ISA, Implementations,
      Software Support
    * Drahflow: Math - Galois-Fields
    * blinry: Einführung in 3D-Shaderprogrammierung mit GLSL und Raymarching

[Aufzeichnungen]: https://stratum0.org/wiki/Vortr%C3%A4ge/Vorbei#2018


Stickecken-Renovierung
----------------------

Die Stickecke wurde aufgeräumt, teilweise neu verputzt, neu gestrichen, und mit
Pegboards zum Hinhängen von Dingen, Beleuchtung zum Sehen von Dingen, sowie
einem Hocker zum Sitzen vor Dingen versehen ([Bild][st]). Ein paar mehr Regale
darüber sind noch geplant, aber noch nicht umgesetzt.

[st]: https://twitter.com/Pecc0r/status/953240428010201088


Hacken Open Air
---------------

Es wird ein [HackenOpenAir] 2::18 geben!
Nach einem initialen Treffen haben wir bekannte und neue Locations sondiert und
uns letztendlich für eine Location entschieden.
Weitere Infos werden folgen.

[HackenOpenAir]: https://hackenopenair.de/


Misc
----

* Im Wiki gibt es jetzt einen kleinen Suche/Biete/Marktplatz, den [Marketspace].
* Die ganzen ehemals im Frickelraum herumfliegenden Poster befinden sich jetzt
  auf einer Rolle im Subraum aufgehängt neben dem Verstärker.
* Der 25kg-Sack Pizzamehl vom Easterhegg wurde vernichtet, der 25kg-Sack Type
  405 ist noch mindestens halb voll.
* Wir haben eine neue (kleinere) Leiter, steht aktuell vor dem Flurschrank,
  langfristig vermutlich im Flurschrank.
* Im Space befindet sich jetzt ein [Luxmeter], sowie ein Dezibelmeter.

[Marketspace]: https://stratum0.org/wiki/Marketspace
[Luxmeter]: https://twitter.com/DooMMasteR/status/963855939475595268


Termine
-------

* Mo, 26.02. 18:00: Malkränzchen, offener Kreativabend
* Di, 27.02. 17:30 - 20:00: 3D-Druck(er) Sprechstunde
* Mi, 28.02. 19:00: Freifunk-Treffen
* Do, 01.03. 19:00 - 23:00: Captain's Log
* So, 04.03. 15:00: OpenStreetMap: Einführung und gemeinsames HOTOSM mapping
* Mo, 05.03. 18:00: Malkränzchen, offener Kreativabend
* Di, 06.03. 18:00: Coptertreffen, Kopter Meeting im Stratum0
* Mi, 07.03. 19:00: Freifunk-Treffen
* Do, 08.03. 18:30: Digital Courage Braunschweig, offenes Ortsgruppentreffen

Willst du diesen Newsletter mitgestalten? Dann schicke deinen kurzen Beitrag an
<kontakt@stratum0.org>.

Happy Hacking!

