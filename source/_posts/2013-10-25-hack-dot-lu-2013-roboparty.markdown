---
layout: post
title: "Hack.lu 2013: Roboparty"
date: 2013-10-25 14:04
comments: true
author: valodim
categories: [english,ctf,writeup,hack.lu13]
---

<span style="font-family: Iceland,sans; font-size: 18px;">Robot
LHCH is happy.  He made it into the castings for the **tenth roman musical**.
He even is so happy that he went on the Oktoberfest to drink some beer.
Unfortunately it seems that he drank too much so now he is throwing up part of
his source code.  Can you decipher the secret he knows?</span>

<span style="font-family: Iceland,sans; font-size: 18px; color: red;">Warning:
Viewing this page is not recommended for people that suffer from epilepsy. We
are dead serious.</span>

<span style="font-family: Iceland,sans; font-size: 18px;">And here is your
totally eye-friendly challenge: \\
https://ctf.fluxfingers.net/static/downloads/roboparty/index.html</span>

<!-- more -->

Besides the eye cancer, the page contains a [midi file] linked in the html.
Smells like an [esolang] already. Some digging yields [Musical X], which is a
perfect match for an unimplemented lang taking notes as its input.
<span style="font-size: x-small">"Tenth Roman Musical", get it?</span>

We can extract notes from the midi file using [lilypad]. All that's left
is implementing the language:

``` python Musical X

import collections
import sys

order = [ "e4", "f4", "g4", "a4", "b4", "c5", "d5", "e5", "f5", "g5", "a5", "b5", "c6", "d6", "e6", "f6", "g6", "a6", "b6" ]

lilycode = [
        "c5", "d5", "e5", "f5", "g5", "a5", "b5", "c6", "d6", "a5", "b5", "c5", "c6",
        "g5", "c5", "g5", "a5", "b5", "c6", "d6", "e6", "b5", "d6", "d5", "e5", "f5",
        "c5", "c6", "f5", "c6", "b5", "c5", "c6", "g5", "a5", "e5", "d5", "c6", "f5",
        "c5", "g5", "f5", "c5", "c6", "g5", "d6", "d5", "e5", "f5", "g5", "d5", "c5",
        "c6", "g5", "f5", "c6", "d6", "a5", "g5", "d5", "c5", "g5", "a5", "e5", "g5",
        "f5", "c5", "c6", "a5", "g5", "c5", "c6", "f5", "c6", "d6", "e6", "b5", "a5",
        "g5", "d5", "a5", "b5", "e5", "e6", "b5", "c5", "c6", "g5", "b5", "c6", "c5",
        "d5", "e5", "f5", "g5", "a5", "b5", "c5", "c6", "g5", "f5", "d5", "d6", "a5",
        "e5", "g5", "d5", "c5", "g5", "d5", "f5", "g5", "a5", "b5", "c6", "d6", "d5",
        "e5", "f5", "g5", "d5", "a5", "f5", "c5", "c6", "b5", "c5", "c6", "g5", "a5",
        "b5", "c5", "c6", "g5", "f5", "c5", "c6", "g5", "b5", "c6", "d6", "a5", "e6",
        "e5", "e5", "e6", "e5", "e6", "e5", "e6", "e5", "f5", "g5", "a5", "b5", "c6",
        "d6", "e6", "b5", "a5", "e5", "f5", "c6", "c5", "g5", "d6", "d5", "e5", "f5",
        "c5", "b4", "b5", "c5", "c6", "g5", "a5", "e6", "b5", "e5", "b4", "b5", "c5",
        "c6", "f5", "a5", "d5", "a5", "f5", "c5", "c6", "g5", "b5", "c5", "c6", "g5",
        "e5", "f5", "c6", "d6", "e6", "b5", "d6", "a5", "b5", "c6", "c5", "g5", "e5",
        "e6", "b5", "d6", "a5", "e6", "d6", "d5", "a5", "g5", "d5", "c5", "g4", "b4",
        "b5", "c5", "c6", "g5", "d6", "a5", "e5", "b5", "a5", "g5", "d6", "a5", "e5",
        "b5", "a5", "g5", "d6", "a5", "e5"
    ]

commands = []
output = []

last = order.index(lilycode[0])
for i in lilycode[1:]:

    cur = order.index(i)
    interval = cur-last
    if interval != 0:
        interval += (1 if interval > 0 else -1)
    # if abs(interval) == 8:
        # continue

    commands.append([interval,[last,cur]])
    last = cur

# print commands
# sys.exit(0)

cur = 0

# There are many infinite tapes as in Brainfuck with values 0-255, each tape
# has a separate pointer, and each tape is named with one note.

# The initial tape is the note at the beginning of the program.
tapes = {}
tps = collections.defaultdict(int)

pointer = commands[0][1][0]
# initial tape
tapes[pointer] = collections.defaultdict(int)

print "set of commands:", set(map(lambda x: x[0], commands))
# exit()

while True:

    if cur >= len(commands):
        print "ok!"
        break

    cmd, firstsecond = commands[cur]
    first, second = firstsecond
    cur += 1

    # print "pointer: ", pointer, "tps[pointer]", tps[pointer]
    # print tps

    print str(cur).rjust(4), " ", cmd, first, second

    # skip unisons and octaves
    if cmd == 0:
        continue

    if abs(cmd) == 8:
        pass
    elif cmd == 2:
        # Increment value at pointer (mod 256).
        tapes[pointer][tps[pointer]] = (tapes[pointer][tps[pointer]]+1)%256
    elif cmd == -2:
        # Decrement value at pointer (mod 256).
        tapes[pointer][tps[pointer]] = (tapes[pointer][tps[pointer]]-1)%256
    elif cmd == 3:
        # Move tape pointer one space forward.
        tps[pointer] += 1
    elif cmd == -3:
        # Move tape pointer one space backward.
        tps[pointer] -= 1

    elif cmd == 4:
        # Input value at pointer.
        print "hum? input required..."
    elif cmd == -4:
        print tapes
        # Output value at pointer.
        print "output tape", pointer, "pos", tps[pointer], "dec", tapes[pointer][tps[pointer]], "chr" # chr(ord('a')+tapes[pointer][tps[pointer]])
        output.append(tapes[pointer][tps[pointer]])

    elif cmd == 5:
        # Select tape indicated by second note.
        pointer = second
        pointer = pointer % 7
        if pointer not in tapes:
            tapes[pointer] = collections.defaultdict(int)
    elif cmd == -5:
        # Move to initial position of current tape.
        tps[pointer] = 0

    # those don't occur!
    elif cmd == 6:
        # If value at pointer is non-zero, then search backward to command with
        # a first note the same note as the second note of this command.
        if tapes[pointer][tps[pointer]] != 0:
            cur -= 2
            while commands[cur][1][0] != second:
                cur -= 1
                if cur < 0:
                    print "error!"
                    break

    # those don't occur!
    elif cmd == -6:
        # If value at pointer is zero, then search forward to command with a
        # first note the same note as the first note of this command.
        if tapes[pointer][tps[pointer]] == 0:
            while commands[cur][1][0] != first:
                cur += 1
                if cur >= len(commands):
                    print "error!"
                    break

    # only one occurence, with -5 (skip)
    elif cmd == 7:

        cmd, firstsecond = commands[cur]
        first, second = firstsecond
        cur += 1

        # skip unisons and octaves
        if cmd == 0 or cmd == 8:
            continue

        print str(cur).rjust(4), "aux", cmd, first, second

        if cmd == 2:
            # tape value under pointer +64 mod 256
            tapes[pointer][tps[pointer]] = (tapes[pointer][tps[pointer]]+64)%256
        elif cmd == -2:
            # tape value under pointer -64 mod 256
            tapes[pointer][tps[pointer]] = (tapes[pointer][tps[pointer]]-64)%256

        elif cmd == 3:
            # tape pointer +64
            tps[pointer] += 64
        elif cmd == -3:
            # tape pointer -64
            tps[pointer] -= 64

        elif cmd == 4:
            # Change notes not of this key to round up instead of down.
            print "+7/4 not implemented"
        elif cmd == -4:
            # Change notes not of this key to round down instead of up.
            print "+7/5 not implemented"

        elif cmd == 5:
            # Select tape indicated by first note.
            pointer = first
        elif cmd == -5:
            # skip next command
            cur += 1

        elif cmd == 6:
            # If value at pointer is even, then search backward to command with
            # a first note the same note as the second note of this command.
            if tapes[pointer][tps[pointer]] % 2 == 0:
                cur -= 1
                while commands[cur][1][0] != second:
                    cur -= 1
                    if cur < 0:
                        print "error!"
                        break
                print "jumped to ", cur


        elif cmd == -6:
            # If value at pointer is odd, then search forward to command with a
            # first note the same note as the first note of this command.
            if tapes[pointer][tps[pointer]] % 2 != 0:
                while commands[cur][1][0] != first:
                    cur += 1
                    if cur >= len(commands):
                        print "error!"
                        break
                print "jumped to ", cur


        elif cmd == 7:
            print "auxiliary +7 is (RESERVED)"
            pass
        elif cmd == -7:
            print "auxiliary -7 is (RESERVED)"
            pass


    elif cmd == -7:
        if second != 5:
            print "!!!!!!!!!!! bad -7"
        # change "key"? doesn't matter.
        pass

    else:
        print "unknown interval:", cmd

print output
print ''.join(map(str, output))
# print ''.join(map(lambda x: chr( (ord('a')+x)), output))
```

Half that stuff isn't even needed, the midi file doesn't implement any loops.
We got the hint that all key changes should have the C from the beginning as
second note.

Output yields 52 digits: 8952894432985197117116491021177632771179049107333333
As the 333333 at the end suggests, this is (variable length) decimal ascii.

flag: Y4Y, b3aut1fuL MuZ1k!!!

[midi file]: http://valodim.stratum0.net/ctf/hacklu13/robotparty.mid
[esolang]: http://esolangs.org/
[Musical X]: http://esolangs.org/wiki/Musical-X
[lilypad]: http://lilypond.org/
