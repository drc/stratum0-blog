---
layout: post
title: "Stratumnews Vol. 23"
author: Stratumnews-Team
date: 2017-09-28 18:50
comments: true
categories: [german,stratumnews]
---

Hallo Entitäten,

an dieser Stelle wieder eine zwei(±n)wöchentliche Zusammenfassung for all things space:

<!-- more -->

HOA17
-----

Es wurde erfolgreich gehackt. Der Space ist wieder im Normalzustand
([Beweisfoto][ha]). Es ist eine Litfasssäule im Space gespawned, sie braucht
mehr LEDs.

Fürs HOA haben wir bei Belnet einige Netzwerkkabel eingesammelt, insbesondere
von 5m Kabeln haben wir derzeit $sehr viele, falls jemand für Privatgebrauch
welche braucht, bedient euch.

Einige Getränke, insbesondere Flora Mate, Club Mate und Premium Cola sind beim
HOA übrig geblieben. Diese hat die Matekasse/der Space mittlerweile von der UG
abgekauft und können jetzt im normalen Matekassenbetrieb verwendet werden.

[ha]: https://matrix.stratum0.org/_matrix/media/v1/download/stratum0.org/oayWlFBvGlNtysVEkFzkUWDb


Mitgliederversammlung
---------------------

Wir haben erfolgreich unsere Mitgliederversammlung 2017 abgehalten. Der
bestehende Vorstand bleibt bestehen, eine Entlastung wurde ihm wegen
Unklarheiten in der Buchführung aber noch versagt. Es wurde beschlossen, dass
die nächste Mitgliederversammlung am 14.01.2018 stattfinden soll. Alle
wichtigen Materialien werden demnächst auf der [Wiki-Seite][mv] nachgeliefert.

Hintergrund und Ursache dieser MV war, dass wir unseren regulären MV-Termin aus
Gründen von Dezember auf Januar verschieben wollten. Damit hätte es aber in
diesem Jahr keine MV gegeben, was satzungsmäßig unzulässig wäre; und der
Vorstand, der nur auf ein Jahr gewählt ist, wäre einen Monat kommissarisch im
Amt. Daher diese Mitgliederversammlung.

[mv]: https://stratum0.org/wiki/Mitgliederversammlung_2017-09-24


Drucker im Space
----------------

Der Lexmark C912 im Flur wurde aus Vereinsgeld mit einer neuen Fixierrolle,
Fixieröl und schwarzem Toner wieder auf Vordermann gebracht, und kann jetzt
wieder genutzt werden. (Wie? [Steht im Wiki!][le])

Die Drucker im Space können prinzipiell für private Zwecke genutzt werden. Da
der Verein dafür aber die Kosten trägt, bitten wir euch, in dem Fall 5 Cent pro
ausgedruckter Seite in die Spendenbox im Flur zu tun.

[le]: https://stratum0.org/wiki/Lexmark_C912


lost+found
----------

Die Werkstattwand vermisst eine blaue Kombizange, einen 13er- und einen 17er
Ringmaulschlüssel.

Die Löt- und Elektronikecke vermisst ein Schraubendreherset in einer grünen
Plastikbox.


Misc
----

Der Space ist jetzt bei [Amazon Smile gelistet][Stratumsmile].
Wenn ihr also ohnehin bei Amazon einkauft, tut dies doch einfach über Smile,
wenn dann der Space als geförderte Organisation ausgewählt ist, bekommen wir
0,5% vom Umsatz ab.

[Stratumsmile]: https://smile.amazon.de/ch/14-208-04224

Wir haben vom EasterHegg 2015 immer noch einige Tassen und gelaserte
Bestecksets über, diese sind nun über die Matekasse erwerbbar.

Die Bernardo-Standbohrmaschine wurde gekauft und sollte demnächst™ ankommen.

Diesen Monat gab es wieder relativ viele Vorträge, diesmal über MQTT, Labdoo,
CUPS, ISO 639 & ISO 3166, und das HOA-Internet-Setup.
[Shownotes dazu wie immer im Wiki](https://stratum0.org/wiki/talks-2017-09-14).

Unser Blog-Setup ist jetzt noch komfortabler geworden: nach einem `git push` auf
den Branch namens `preview` erscheint der Inhalt nach kurzer Wartezeit auf
<https://sandbox.stratum0.org/blog/>. 

Wir wurden von Mitmietern im Haus darauf hingewiesen, dass das Grillen auf dem
Dach ihre Brandmeldeanlage auslösen könnte und dass das Hantieren mit Feuer auf
dem Dach eine zusätzliche und unnötige Brandgefahr darstellt.  Bitte denkt also
daran, in Zukunft nichts auf dem Dach zu machen, was eine größere Menge Rauch
produziert, Feuer beinhaltet oder einen Brandmelder auslösen könnte – und nicht
vom Dach zu fallen.


Termine
-------

*  Do, 28.09. 19:00 - 23:00: Captain's Log
*  Mo, 02.10. 18:00: Malkränzchen
*  Di, 03.10. 17:30 - 20:00: 3D-Druck(er) Sprechstunde
*  Di, 03.10. 18:00: Coptertreffen, Kopter Meeting im Stratum0
*  Mi, 04.10. 16:00: Schreibrunde
*  Mi, 04.10. 19:00: Freifunk-Treffen
*  Do, 05.10. 18:30: Digital Courage Braunschweig, offenes Ortsgruppentreffen
*  Do, 05.10. 19:00 - 23:00: Anime Referat
*  Mo, 09.10. 18:00: Malkränzchen
*  Di, 10.10. 17:30 - 20:00: 3D-Druck(er) Sprechstunde
*  Di, 10.10. 19:00: Vorstand Arbeitstreffen
*  Mi, 11.10. 16:00: Schreibrunde
*  Mi, 11.10. 19:00: Freifunk-Treffen
*  Do, 12.10. 19:00 - 23:00: Captain's Log
*  So, 15.10. 15:00: CoderDojo Vorbereitungsbasteln
*  Mo, 16.10. 18:00: Malkränzchen
*  Di, 17.10. 17:30 - 20:00: 3D-Druck(er) Sprechstunde
*  Di, 17.10. 18:00: Coptertreffen, Kopter Meeting im Stratum0
*  Mi, 18.10. 16:00: Schreibrunde
*  Do, 19.10. 18:30: Digital Courage Braunschweig, offenes Ortsgruppentreffen
*  Do, 19.10. 19:00 - 23:00: Anime Referat
*  Sa, 21.10. 14:00: CoderDojo
*  Sa, 11.11. 13:37 bis So, 12.11. 18:00:  #Coldney17
*  So, 12.11. 15:00:  #Coldney17-Aufräumtreff
*  Mi, 27.12. bis Sa, 30.12.: 34c3, Leipzig


Willst du diesen Newsletter mitgestalten? Dann schicke deinen kurzen Beitrag an
<kontakt@stratum0.org>.

Happy Hacking!
