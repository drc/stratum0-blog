---
layout: post
title: "Freifunk CI"
date: 2017-08-16 12:00
comments: true
author: chrissi^
categories: [german, freifunk]
---

Die Freifunk-Firmware
------

Im Braunschweiger Freifunk-Netz wird [Gluon] als Firmware auf den Routern unseres Netzes eingesetzt. Gluon ist als Framework zum Erstellen von WLAN-Mesh-Netwerken gedacht. Die Entwicklung einer Firmware für eine Community teilt sich in zwei Abschnitte:

* Gluon selbst wird von einer Community auf Github entwickelt. Diese Community leistet dabei den großen Teil des Aufwandes. Gluon wird als ein sozusagen schlüsselfertiges Framework ausgeliefert. Es ist alles vorbereitet und es fehlen nur noch die lokalen Spezialitäten.
* Diese lokalen Spezialitäten sind der zweite, dezentrale Teil der Gluon-Entwicklung. In diesem Schritt wird Gluon um die so genannte [Site] erweitert, die die jeweilige Firmware konfigurieren.

Bisher haben wir in Braunschweig, abgesehen von wenigen Ausnahmen, immer am zweiten Abschnitt der Gluon-Entwicklung gearbeitet und Gluon im Wesentlichen eingesetzt. Auf dem [Freifunk-Reboot-Workshop] (Anmeldung notwendig) haben wir nun aber beschlossen auch tiefer in das Design des Netzes einzusteigen. Teil dieses Prozesses wird es auch sein tiefere Änderungen am Gluon vornehmen zu müssen.


Was es zu Testen gibt
-----

Um auf diesen Schritt vorbereitet zu sein ist die Idee des Freifunk CI entstanden: Freifunk-geeignete Router sollen mit Fernsteuertechnik ausgestattet werden um auf diesen automatisiert Tests gegen ein Firmware-Image fahren zu können.

Damit sind wir in der Lage drei Arten von Tests durchzuführen:

* Für die Pflege des aktuellen Systems können normale Gluon-Images vor einem Release auf Funktion getestet werden. Somit wird sichergestellt, dass alle freigegebenen Firmwaren einen Mindest-Funktionsumfang besitzen.
* Für die Entwicklung eines neuen Systems können ebenfalls Gluon-Images gegen geforderte Funktionen getestet werden.
* Für den operativen Betrieb des Netzes kann mit realen Routern getestet werden ob Dienste, wie z.B. der Internetzugang, verfügbar sind.

Der Aufbau
----

Das Freifunk-CI hat dabei aktuell folgenden Aufbau:

* Es sollte ein Gerät verwendet werden, wie es auch häufig im Freifunk eingesetzt wird. Für die ersten Prototypen haben wir uns für einen TP-Link WR841ND entschieden. Dieses Gerät hat trotz der schlechten Hardware-Ausstattung im Freifunk Braunschweig eine weite Verbreitung gefunden und scheint uns daher ein geeignetes Testobjekt zu sein.
* Als Testserver kommen Einplatinencomputer zum Einsatz. In unserem Fall: Raspberry Pi 3
* Die serielle Schnittstelle des Routers wird am Testserver angeschlossen. So lässt sich der Bootvorgang über Bootloader und Kernel bis in die Konsole beobachten und selbst gebrickte Geräte automatisiert wiederherstellen.
* WLAN- und Reset-Knopf, sowie die Spannungsversorgung werden für den Testserver schaltbar gemacht.
* Das Client-LAN des Routers wird über einen USB-Ethernet-Adapter an den Testserver angeschlossen.
* Der WAN-Anschluss des Routers und LAN-Anschluss des Testservers werden mit dem normalen Netzwerk verbunden.

Der Prototyp macht seinem Namen alle Ehre:

{% fancyalbum 200x200 %}
images/posts/2017-08-16-freifunk-ci/router.jpg: Das Test-Setup: Router mit Raspberry und Switch-Platine
images/posts/2017-08-16-freifunk-ci/switch1.jpg: Switch-Platine Oberseite
images/posts/2017-08-16-freifunk-ci/switch2.jpg: Switch-Platine Unterseite
{% endfancyalbum %} 

Auf der Software-Seite soll [Labgrid] zur Testautomatisierung zum Einsatz kommen. Evenutell wird dies später mit Jenkins als Build-Controller kombiniert. Aktuell sind die notwendigen Treiber für den Testaufbau und Gluon als Firmware in der Entwicklung. 


[Gluon]: http://gluon.readthedocs.io/en/v2017.1.x/
[Site]: https://github.com/ffbs/site-ffbs
[Freifunk-Reboot-Workshop]: http://lists.freifunk.net/mailman/private/braunschweig-freifunk.net/2017-April/001175.html
[Labgrid]: https://github.com/labgrid-project/labgrid


