---
layout: post
title: "Replacing a Husqvarna Viking Designer Topaz 30 Needle Holder"
author: rohieb
date: 2016-10-30 00:25
comments: true
categories: [english,embroidery,sewing]
---

We have a [Husqvarna Designer Topaz 30 embroidery machine][wiki], and the needle
holder recently broke in two. The [replacement part] only was about 30€.
I tried searching for a service manual for that machine, but the results are
really sparse and you get to the strange sites really soon… But it turned out
that the replacement is very easy, you'll only need a 1.5mm hex key.

1. Turn the needle holder to the “down” position using the hand wheel, remove the sewing foot and the hand screw for tightening the needle in the holder.
2. Use the 1.5mm hex key to open the little set screw on the left side of the needle holder. Remove the needle holder by sliding it downwards. While you're at it, grab a paper towel and clean the rod from lint.
3. The rod on which the holder sits is actually a hollow pipe. The old holder had a little groove at the lower end which gripped the pipe tightly when the set screw was put in place, and also the hole for the needle was separated from the fixture hole. The new replacement part didn't have such a groove at the bottom, and the needle hole was not separated by a wall. Instead the new kit came with a little cap thingy that probably helps center the tube and acts as a stopping point when inserting the needle. I just put the cap thingy into the needle holder, the bulge facing upwards. Works for me.
4. Put the set screw from the old holder into the upper hole (the replacement kit didn't come with a set screw), and the tightening screw for the needle into the lower hole.
5. Put the holder onto the tube as before, align the set screw so it faces approximately orthogonally to the left, then tighten the set screw.
6. Put a needle and a sewing foot in, and make a few test stitches.

Pictures are better than 253 words:

{% fancyalbum 200x200 %}
images/posts/2016-10-30-replacing-a-needle-holder/old-holder.jpg: Old needle holder in the “down” position, with the set screw in the upper hole. The needle tightening screw has already been removed.
images/posts/2016-10-30-replacing-a-needle-holder/kit-contents.jpg: All parts. From left to right: old set screw, needle tightening screw, the new cap thingie, the new needle holder, and the old needle holder, split in two. On the latter you can see the groove at the bottom of the fixture hole.
images/posts/2016-10-30-replacing-a-needle-holder/new-holder.jpg: View into the new needle holder. Note that the two holes are not separated.
images/posts/2016-10-30-replacing-a-needle-holder/new-holder-with-cap-thingie.jpg: New needle holder with the cap thingie in place, bulge facing upwards.
images/posts/2016-10-30-replacing-a-needle-holder/new-holder-with-set-screw.jpg: New needle holder with old set screw
images/posts/2016-10-30-replacing-a-needle-holder/new-holder-on-fixture.jpg: New needle holder on the fixture, aligned and set screw tightened
images/posts/2016-10-30-replacing-a-needle-holder/finished.jpg: Voilà, it works!
{% endfancyalbum %} 

[wiki]: https://stratum0.org/wiki/Stickmaschine
[replacement part]: http://www.naehmaschinen-ersatzteile.com/Verschiedenes/Nadelhalter/Nadelhalter-fuer-Pfaff/Nadelhalter-Husqvarna-versch-Modelle.html
