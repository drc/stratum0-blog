---
layout: post
title: "Zeitabgleich: CCCAC"
date: 2013-10-26 14:09
comments: true
author: valodim
categories: [german,zeitabgleich,cccac,hack.lu13]
---

Wie kürzlich erwähnt, bandelt unser [CTF Team] momentan mit dem Team vom
[CCC Aachen] an. Bisher beschränkte sich die Zusammenarbeit dabei allerdings
auf Kollaboration online. Anlässlich des [Hack.lu CTF 2013][hack.lu13] hat
sich eine Division unseres Teams bestehend aus DooMMasteR, tsuro, comawill,
Kasalehlia und meiner Wenigkeit auf den Weg nach Aachen gemacht, um vor Ort
gemeinsam als Stratum Auhuur teilzunehmen.

<!-- more -->

Wir machten uns Montag Abend auf den Weg, um zu Beginn des CTFs am Dienstag um
10 Uhr ausgeschlafen zu sein. Nach gut 4 Stunden Fahrt reichte die Anweisung
"Vor Zentis rechts, dann gleich links, bis zur roten Tür," um unser Ziel zu
finden. Die Aachener waren so freundlich uns privat unterzubringen, womit
unsere Schlafplätze schnell geklärt waren.

Was wir "Space" nennen, wird dort aus naheliegenden Gründen "Club" genannt. Man
wirft uns ab und zu vor, bei uns in den Space zu kommen sei eher "wie bei Tante
zu Besuch" als in einen Hackerspace. Das kann man dort sicherlich nicht sagen -
der Club hat ein sehr originales, um nicht zu sagen rustikales, flair. 

Der Weg dorthin führt durch einen großen Industriekomplex, vorbei an einer
Vielzahl chemischer Gerüche und arbeitender Bevölkerung (auch nachts). Der Club
selbst hat einen großen Raum (sowas wie $75\mathrm{m}^2$), der im Grunde eine
Industriehalle plus Sofata, Arbeitstische, einiger Regale und LEDs ist. Eine
Küche sucht man vergeblich, dafür findet man immer jemanden zum gemeinsam Pizza
bestellen. Die Fenster sind einfach verglast bei für mein Empfinden sehr
angenehmer Deckenhöhe, was aber niemanden stört da die Heizkosten sowieso
pauschal abgerechnet werden.

Als wir ankamen waren an die 10 Leute dort. Es lief das Intro der
Gummibärenbande, wir tauschten uns aus über Vereinsfoo, #Merkelphone, die
besten und schlechtesten Aufgaben der letzten CTFs, uns wurde [das Logo]
erklärt (33313b, verstehste?) und die Zeit ging bei Smalltalk schnell vorbei.

Neben den CTF Spielern herrschte reges Treiben im Club. Am Mittwoch waren
zwischenzeitlich sicherlich insgesamt 25 Leute anwesend, die Mehrheit zum Bier
trinken und Socializen. Davon bekamen wir, hochkonzentriert auf die Challenges,
allerdings nur am Rande etwas mit.

Wir verbrachten fast unsere gesamte Zeit damit, auf den ein oder anderen
Bildschirm zu starren, und an den Aufgaben zu arbeiten. Den Dienstag und
Mittwoch lang konnten wir uns lange Zeit an der Spitze des Scoreboards halten,
am Ende erreichten wir allerdings "nur" den dritten Platz. Ich kann trotzdem
sagen, das Experiment war ein voller Erfolg! Wir hätten getrennt gespielt
sicherlich die ein oder andere Aufgabe weniger geschafft, insbesondere lange
Aufgaben wie [Wannabe] haben von dem so möglichen Teamwork profitiert. Man
konnte eine "team size" ins Profil eintragen - für rein statistische Zwecke
- und diese Zahl mussten wir zwischenzeitlich auf eine beachtliche 12 (Zwölf)
hochkorrigieren!

Wir hatten auf jeden Fall eine Menge Spaß, und freuen uns darauf die Saison
2014 als Stratum Auhuur zu spielen. Top 10 werden wohl drin sein! =)

<span>Burgerparty \o/</span>{: style="font-size: x-small;" }

[CTF Team]: https://stratum0.org/blog/blog/2013/09/27/about-writeups-and-ctfs/
[CCC Aachen]: http://aachen.ccc.de/
[hack.lu13]: https://ctf.fluxfingers.net
[Wannabe]: https://stratum0.org/blog/blog/2013/10/25/hack-dot-lu-2013-wannabe/
[das Logo]: http://aachen.ccc.de/images/cccac_logo_bw_kleiner.png

