---
layout: post
title: "Mitgliederversammlung 2014-12-07: tl;dr"
date: 2014-12-11 01:22
comments: true
categories: [german, verein]
---

**Update:** Das vollständige Protokoll ist im [Wiki] zu finden.

Die [diesjährige Mitgliederversammlung][mv2015] am 7. Dezember ist vorbei, hier
eine kurze Übersicht, was passiert ist:
<!--more-->

Allgemeines
-----------
* Es waren 27 stimmberechtige Mitglieder (41%) und 2 Gäste anwesend.
* Es wurden mehrere 100 Gramm Waffeln verzehrt (danke, Sigrun!)
* Außerdem wurden 65 Flaschen Getränke getrunken (danke, [@schakko][schakko]!)

Finanzielles
------------
(Alle Zahlen beziehen sich auf den Zeitraum 1. Dezember 2013 bis 1. Dezember
2014)

* Der Verein hat im letzten Jahr unterm Strich 21.048,23&nbsp;€ eingenommen,
	21.344,21&nbsp;€ ausgegeben und damit 295,98&nbsp;€ Verlust gemacht.
	Angesichts der umgesetzen Projekte, insbesondere dem noch laufenden Ausbau im
	[Space 2.0][space2.0], ist das im gesunden Bereich.
* Der Stromverbrauch steigt an und könnte bis zu 11.000 kWh im Jahr betragen.
	Größere Verbraucher sind die Lampen und die Geräte, die die Grundlast
	verursachen.
* Der Verein ist rückwirkend für das Jahr 2014 vom Finanzamt als
	steuerbegünstigt anerkannt worden. Das bedeutet, dass ihr eure
	Mitgliedsbeiträge und Spenden an den Verein von der Steuer absetzen könnt.
	Bis 200&nbsp;€/Jahr glaubt das Finanzamt euren Kontoauszügen, darüber hinaus
	kann unser Schatzmeister euch Zuwendungsbescheinigungen ausstellen.
* Die Mitgliederentwicklung stagniert langsam, ist aber noch "okay". Derzeit hat
	der Verein 66 ordentliche und 5 Fördermitglieder.
* Die Kassenprüfung hat keine vorsätzlichen Fehler in der Buchhaltung
	festgestellt und hat dazu geraten, den Vorstand zu entlasten.
* Die Mitgliederversammlung hat den Vorstand einstimmig entlastet.
* Es wurde eine Rücklage von 3000&nbsp;€ als Puffer für längerfristige
	Verbindlichkeiten (Miete, Verträge, etc.) beschlossen. Der Beschluss durch die
	MV war im Prinzip nur eine fiskalische Formsache, die Rücklage wurde in der
	Vergangenheit vom Vorstand schon so kommuniziert und existierte als solche
	auch schon länger in den Büchern.

{% fancyalbum 300x300 %}
images/posts/2014-12-11-mitgliederversammlung-2014-12-07-tl-dr/guthaben.png: Guthabenverlauf
images/posts/2014-12-11-mitgliederversammlung-2014-12-07-tl-dr/finanzuebersicht.png: Finanzübersicht
images/posts/2014-12-11-mitgliederversammlung-2014-12-07-tl-dr/mitglieder.png: Mitgliederentwicklung
images/posts/2014-12-11-mitgliederversammlung-2014-12-07-tl-dr/strom.png: Stromverbrauch
{% endfancyalbum %}

Ausführlichere Infos könnt ihr dem [Finanzbericht 2015][finanzpdf] entnehmen.

Personelles
-----------
* Nachdem die Legislaturperiode des alten Vorstand zu Ende ging und einige
	Vorstandsmitglieder ihr Amt nicht weiterführen wollten, wurde ein neuer
	Vorstand gewählt. Für das nächste Jahr sind dies:
  - larsan (neuer 1. Vorsitzender, gewählt mit 96% Zustimmung)
  - rohieb (wie bisher stellv. Vorsitzender, gewählt mit 73% Zustimmung)
  - chrissi^ (wie bisher zuständig für Cash Moneys, gewählt mit 100% Zustimmung)
  - sowie comawill (92%), Kasalehlia (84%) und hanhaiwen (73%) als
    Beisitzende.

Sonstiges
---------
* Zum Wechsel des Stromanbieters wurde kein konkreter Beschluss gefasst. Der
	Vorstand wurde aber mit großer Mehrheit dazu beauftragt, auch weiterhin auf
	Ökostrom zu setzen.
* Es werden noch aktive Unterstützer für die [EasterHegg][eh15]-Orga gesucht,
	Interessierte dürfen gerne bei den regelmäßigen Treffen vorbeischauen.
* Eine mögliche Anerkennung des RaumZeitLabors als Außenstelle des Stratum 0
	wurde auf die nächste Versammlung verschoben.

[schakko]: https://twitter.com/schakko/status/454351287719849985
[space2.0]: https://stratum0.org/blog/categories/space2-dot-0://stratum0.org/wiki/Arbeitsgruppe:Space_v2.0
[mv2015]: https://stratum0.org/wiki/Mitgliederversammlung_2014-12-07
[finanzpdf]: https://stratum0.org/mediawiki/images/e/e2/Finanzbericht_2014.pdf
[eh15]: http://eh15.easterhegg.eu
[Wiki]: https://stratum0.org/wiki/Datei:Mitgliederversammlung_2014-12-07.pdf
