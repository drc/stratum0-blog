---
layout: post
title: "1. CoderDojo im Stratum 0"
date: 2014-07-19 20:15
comments: true
author: larsan & comawill
categories: [CoderDojo,Programming,german]
---
{% fancyimage left,nocaption images/posts/2014-07-19-coderdojo/coderdojo-bs.png 150x150 CoderDojoBS %}

Heute am Samstagnachmittag trafen sich 8 Kinder und Jugendliche bei uns im Stratum 0, um gemeinsam Programmieren zu lernen.
Innerhalb von drei Stunden erstellten die Kinder Spiele und Animationen, zum Teil ohne jegliche Vorkenntnisse im Bereich Programmieren.

<!--more-->
<p style="clear:both" />

Mit der kompetenten Einzelbetreuung durch unsere Mentoren konnte jede Teilnehmerin und jeder Teilnehmer seine eigenen Ideen verwirklichen oder sich mit der **Ideenbox** Anregungen geben lassen.
Umgesetzt wurden die meisten Projekte mit der Programmier-Lernplattform [Scratch][scratch].
Dabei entstanden kleine Spiele, wie Flappy-Hippo (ein Flappy-Bird-Klon) und ein Weglaufspiel, viele animierte Kurzgeschichten sowie programmierte Grafiken.


{% fancyimage center images/posts/2014-07-19-coderdojo/1-programming.jpg 500x500 Programmieren im Chillraum %}

{% fancyimage center images/posts/2014-07-19-coderdojo/2-programming.jpg 500x500 Programmieren im Frickelraum %}

Herzlich bedanken möchten wir uns bei Anke und Ersoy für die Organisation des CoderDojos und bei allen Mentoren, die tatkräftig mitgeholfen haben.
Wir bedanken uns ebenfalls bei allen Teilnehmerinnen und Teilnehmern und freuen uns auf das nächste CoderDojo im August.


Das nächste CoderDojo in Braunschweig findet am 30.8 um 14.00 wieder im Stratum 0 statt.
Weitere Infos hierzu finden sich im [Wiki][wiki], auf der [Veranstaltungsseite][coderdojobs] und natürlich auf [Twitter][twitter]

[coderdojobs]: https://zen.coderdojo.com/dojo/820
[scratch]: http://scratch.mit.edu/
[wiki]: https://stratum0.org/wiki/CoderDojo
[twitter]: https://twitter.com/CoderDojoBS
