---
layout: post
title: "Network Services I"
date: 2013-10-07 13:49
comments: true
author: valodim
categories: [german,services,dyndns,cloud]
---

Im Rahmen unserer technischen Infrastruktur werden aktuell ein MediaWiki,
dieser Blog, ein Etherpad Lite, einige Mailinglisten, mehrere Domainnames, ein
Jabber Server sowie diverse weitere [Netzdienste] von unterschiedlichen
Mitgliedern gepflegt. Zur Konsolidierung der bestehenden und Planung weiterer
Dienste gab es vergangenen Samstag (den 05.10.) ein Admin-Treffen mit den
folgenden Ergebnissen und Plänen:

<!--more-->

* Ab sofort steht Mitgliedern ein DynDNS Service auf [stratum0.net] zur
  Verfügung. Dieser läuft auf Basis der [nsupdate.info] Software und kann unter
  anderem über das dyndns Protokoll mittels [ddclient] oder durch eine Fritzbox
  erneuert werden. Um einen Account zu bekommen, bitte an [Shoragan] wenden.
  Für weitere Details zur verwendeten Software und zum Thema DNSSEC lohnt ein
  Blick ins [Wiki][s0nwiki].

* Mittelfristig möchten wir alle internen Dienste, die momentan auf privaten
  Maschinen von verschiedenen Mitgliedern laufen, auf direkt vom Verein
  betriebene Server umziehen. Des Weiteren würden wir unseren Mitgliedern gern
  Dienste wie shells, [pagekite] und (möglichst) sichere E-Mail zur Verfügung
  stellen. Da dies allerdings mit signifikanten monatlichen Serverkosten
  verbunden ist, werden diese Pläne aufgeschoben bis sich unsere finanzielle
  Situation nach dem Umzug in den Space 2.0 stabilisiert hat.

* Zur Unterscheidung "offizieller" Dienste des Vereins von den (geplanten)
  persönlichen unserer Mitglieder, sollen letztere auf der neuen Domain
  [stratum0.net], klar abgegrenzt von unserer Hauptdomain [stratum0.org]
  angesiedelt werden.

Darüber hinaus thematisch relevant:

* Es wird in Kürze eine leistungsfähige Maschine mit einem [OpenStack]
  Interface als Dauerleihgabe von tsuro im Space stehen. Diese wird Mitgliedern
  zur Durchführung rechenintensiver Tasks wie etwa verteiltem Kompilieren mit
  distcc oder Brute-Force Anwendungen für [CTF]s zur Verfügung stehen.


[ddclient]: http://sourceforge.net/p/ddclient/wiki/Home/
[Netzdienste]: https://stratum0.org/wiki/Netzdienste
[stratum0.net]: https://stratum0.net
[s0nwiki]: https://stratum0.org/wiki/Stratum0.net
[stratum0.org]: https://stratum0.org
[nsupdate.info]: http://nsupdate.info
[pagekite]: https://pagekite.net
[openstack]: http://www.openstack.org/
[CTF]: /blog/2013/09/27/about-writeups-and-ctfs/
[Shoragan]: mailto://jluebbe@lasnet.de
