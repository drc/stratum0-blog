---
layout: post
title: "Freifunk für Braunschweig und Umgebung"
date: 2014-09-04 02:56
comments: true
author: larsan
categories: [freifunk, german]
banner:
  img: images/posts/2014-09-04-freifunk/header.jpg
  source: http://de.wikipedia.org/wiki/Datei:Freifunk-Initiative_in_Berlin-Kreuzberg.jpg
  license: CC BY-SA 3.0
  author: Boris Niehaus (JUST)
---

![Logo der Braunschweiger Freifunk-Initiative](/images/posts/2014-09-04-freifunk/Logo_Freifunk_Braunschweig.svg){:width="500"}
{: style="text-align: center;"}

"Was ist Freifunk?" – Ist eine Frage, die uns häufiger gestellt wird. Auf diese Frage gibt es mehrere Antworten, die alle gleichermaßen richtig sind:


<!--more-->
**Freifunk ist ein freies WLAN** – Viele begegnen Freifunk zum ersten Mal, indem sie sich auf ihrem Mobilgerät mit dem offenen WLAN *braunschweig.freifunk.net* verbinden. 
Im Gegensatz zu kommerziellen Dienstleistern, bei denen man nach diesem Schritt abgefangen und zur Zahlung aufgefordert wird, bekommt man im Freifunk-Netz direkt eine Verbindung ins Internet. Keine Abfangseite, keine zeitlichen Beschränkungen, man ist sofort online. Bedingungslos.

Das ist von uns so gewollt, wir mögen eingeschränktes Internet genauso wenig wie ihr. Anbieter von Freifunk-WLANs können alle sein – Privatpersonen, Firmen, öffentliche Einrichtungen, oder Vereine – dazu reicht es, einen entsprechend konfigurierten WLAN-Router bei sich aufzustellen und ihn über den eigenen Internetanschluss mit dem Internet zu verbinden. Der Router stellt dann im *braunschweig.freifunk.net*-WLAN einen Zugang zum Internet bereit, der von deinem eigenen, privaten Netzwerk abgetrennt ist. 

An dieser Stelle kommt häufig der Einwand, dass andere den eigenen Anschluss missbrauchen könnten und dass einem dann sehr bald eine Abmahnung ins Haus flattert. Der Einwand ist berechtigt, denn solange die (mit Deutschland einmalige) Frage der sogenannten *Störerhaftung* nicht geklärt ist, würden auch die meisten von uns nicht ohne weiteres ein offenes WLAN anbieten. Im Freifunk-Netz wird die Störerhaftung umgangen, indem der gesamte Internetverkehr der Freifunk-Router zunächst über verschlüsselte Tunnel auf unseren eigenen Servern (auch *Gateways*) zusammenläuft, und dann von dort aus über einen [VPN][vpn]-Tunnel in ein anderes Land. 

VPNs sind legitime Werkzeuge im Internet: Firmen, Universitäten, und Privatpersonen nutzen sie, um sich von überall auf der Welt mit dem eigenen Netzwerk zu verbinden. Doch selbst wenn wir diese VPNs nicht nutzen würden, wären immer nur unsere Gateways, nie jedoch die privaten Anschlüsse der Aufsteller der Freifunk-Router von *außen* aus dem Internet sichtbar.

**Freifunk ist ein Netzwerk** – Ein freies WLAN wäre langweilig, wenn es bloß Internet bereitstellen würde. 

Alle Geräte, die sich im Freifunk-Netz befinden, können sich gegenseitig sehen. Das ist anders als im Internet, wo sich nur Server gegenseitig *sehen* können und Inhalte an Clients ausliefern; Im Freifunk-Netz kann jeder Client auch gleichzeitig Server sein, also anderen Nutzern Dienste und Inhalte zur Verfügung stellen. Webseiten wären hierbei nur das einfachste Beispiel. 

Doch damit nicht genug: Wenn zwei Freifunk-Router nah genug beieinander stehen, können sie ihre Daten auch direkt über WLAN austauschen, das ist viel schneller als der Weg über das Internet. Wenn mehrere Router in gegenseitiger Reichweite stehen, entsteht ein sogenanntes Mesh-Netzwerk. In einigen Städten ist das Freifunk-Netz soweit gewachsen, dass man mehrere dieser Mesh-Netzwerke über Richtfunkstrecken auch über größere Strecken (mehrere km) miteinander verbunden hat. 

Diese Technik war ursprünglich die Idee hinter Freifunk. Zu den Anfängen – vor etwa 10 Jahren – wollte man damit schlecht mit Internet versorgte Gebiete und Stadtteile anbinden. Als die meisten Regionen dann flächendeckend mit DSL versorgt wurden, verlor die Idee ihre Bedeutung, heute erlebt sie vor dem Hintergrund von überwachten und zensierten Netzwerken einen neuen Aufschwung, indem mit Freifunk ein anonymes, diskriminierungsfreies Netzwerk bereitgestellt wird. 

Doch auch bei der Technik hat sich einiges verändert: Zum Einen sind die Kosten für die notwendige WLAN-Hardware enorm gesunken (die meisten Anwendungsfälle sind mit einem einfachen Router für etwa 17&nbsp;€ bereits abgedeckt), während ihre Leistungsfähigkeit deutlich anstieg. Zum Anderen ist die Software gereift; gab es anfangs noch Probleme, ein stabiles Netzwerk mit einer Hand voll Router aufzubauen, so sind heutzutage verlässliche Netzwerke mit mehreren hundert Routern keine Problem.

**Freifunk ist anonym** – Wie bei allen Providern fallen natürlich auch im Freifunk-Netz Verbindungsdaten an, aber im Gegensatz zu üblichen Providern versuchen wir gar nicht erst, sie zu speichern. Das beginnt damit, dass wir gar nicht wissen, wer sich überhaupt über das Freifunk-Netz mit dem Internet verbindet, es gibt schließlich keine Anmeldung. Auch die weiteren Daten, z.B. welches Gerät sich mit welcher Website verbindet, speichern wir nicht. Wir wollen schließlich auch nicht, dass jemand diese Daten über uns speichert.

**Freifunk ist ein Gemeinschaftsprojekt** – Jeder kann mitmachen. Während Freifunk-Communities in den meisten Städten von einer technisch versierteren Organisation im Kern gestartet werden, sind sie offen und jede/r der/die Interesse hat, kann sich beteiligen. In Braunschweig ist diese *technisch versiertere Organisation* der Hackerspace [Stratum 0][stratum0], ein gemeinnütziger Verein, der in diesem Fall die grundlegende Infrastruktur für die Braunschweiger Freifunk-Initiative bereitstellt. Hauptsächlich betrifft dies eingehende Spenden, die vom Verein verwaltet werden, und die regelmäßigen Treffen, die in den Räumen des Stratum 0 stattfinden. Auch kamen die meisten der ersten Beteiligten aus dem Stratum 0, dies hat sich doch schon kurz nach dem Start der Freifunk-Initiative sehr in die Breite gezogen.

**Wer sollte einen Freifunk-Router bei sich aufstellen?** – Erst einmal natürlich: Jeder! Selbstverständlich gibt es bessere und schlechtere Orte, um einen Freifunk-Router aufzustellen, aber während Frisöre und Arztpraxen (Internet im Wartezimmer!), Cafés, Restaurants, Bäckereien und weitere Positionen nahe an öffentlich Plätzen die offensichtlichsten Favoriten sind, gibt es einige unscheinbarere Situationen, in denen sich ein Freifunk-Router lohnt. WGs und Privathaushalte müssen für Besuch nicht immer das WLAN-Passwort raus suchen (und der Besuch hat nicht automatisch Zugriff auf dein Heimnetzwerk). Du ziehst um, dein Internetanschluss wird erst "nächste Woche" geschaltet, aber dein Nachbar hat Freifunk (oder umgekehrt). Dienste im Internet funktionieren nicht, weil du in Deutschland bist, mit Freifunk hast du automatisch eine IP aus dem Ausland (Schweden oder Niederlande). Etc...

**Wer bezahlt das?** – Den größten Anteil tragen die, die einen Router bei sich aufstellen, nämlich die einmaligen Hardwarekosten für den WLAN-Router, sowie die Internetanbindung (und vernachlässigbar wenig Stromkosten). Auf Seite des Freifunk-Netzes entstehen geringe Kosten für die Infrastruktur (Gateways, VPNs), diese befinden sich momentan im niedrigen zweistelligen Bereich (im Monat) und werden durch [Spenden][spenden] getragen.

**Wie kann ich mitmachen?** – Ob du einen Freifunk-Router bei dir aufstellen möchtest, ein größeres Gebiet, oder Gebäude mit Freifunk versorgen, uns organisatorisch dabei unterstützten, Freifunk in Braunschweig und Umgebung weiter zu verbreiten, oder sogar an der Technik, der Firmware und dem Netz, mitarbeiten möchtest, jede Hilfe ist gern gesehen! Am einfachsten kommst du zu einem unserer Treffen. Dort halten wir auch meist ein paar Freifunk-Router vor, die wir zum Selbstkostenpreis weiter verteilen und wenn gewünscht, direkt einrichten können. Wir koordinieren uns über ein offene [Mailingliste][mailingliste] und einen [IRC-Channel][irc], daneben versuchen wir soviel wie möglich in unserem [Wiki][wiki] zu dokumentieren. Wenn du spontane Fragen hast, ist der IRC-Channel womöglich die beste Anlaufstelle.


{% fancyimage center images/posts/2014-09-04-freifunk/clientstats.png 500x700 Clients über einen Tag verteilt%}
Aktuelle Statistiken sind auf [freifunk-bs.de][plot] zu finden.

[vpn]: https://de.wikipedia.org/wiki/Virtual_Private_Network
[stratum0]: https://stratum0.org
[spenden]: https://stratum0.org/wiki/Spenden
[mailingliste]: http://lists.freifunk.net/mailman/listinfo/braunschweig-freifunk.net
[irc]: https://webchat.freenode.net/?channels=freifunk-bs
[wiki]: https://wiki.freifunk-bs.de/
[plot]: https://freifunk-bs.de/plot.html

