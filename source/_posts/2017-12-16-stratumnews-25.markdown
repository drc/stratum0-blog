---
layout: post
title: "Stratumnews Vol. 25"
author: Stratumnews-Team
date: 2017-12-16 21:10
comments: true
categories: [german,stratumnews]
---

Hallo Entitäten,

an dieser Stelle wieder eine zwei(±n)wöchentliche Zusammenfassung for all things space:

<!-- more -->


Flur- und Frickelraumpflege
---------------------------

Irgendwann letztens wurde der Flur und der Frickelraum mal wieder
defragmentiert, aufgeräumt, gesaugt, gewischt, etc.
Die Drehstühle im Frickelraum wurden nach Kaputtheitsgrad geordnet und teilweise
als aussonderungswürdig markiert (weißer Aufkleber auf der Rückseite). Mag sich
jemand mit Auto mal bereiterklären, die zu entsorgen?


34c3
----

Wir haben eine [Assembly] angemeldet. Es steht großes bevor. Assemblyplanung im
[34c3-Pad] und auf den Planungs-/Vorbereitungsbasteltreffen.

[Assembly]: https://events.ccc.de/congress/2017/wiki/index.php/Assembly:Stratum_0
[34c3-Pad]: https://pad.stratum0.org/p/34c3stratum


MQTT und service-vm
-------------------

Auf der [APU] läuft jetzt eine Service-VM für rudimentäre
Spaceinfrastrukturdinge.  Der MQTT-Server ist bereits dahin umgezogen,
MQTT-Dinge finden sich also jetzt unter `service-vm[.s0]` statt
`nastratum[.s0]`.

Die Netzwerkgerätezählung flooded jetzt nicht mehr das Spacenetz vom powerberry
aus, sondern guckt auf der LEDE-VM direkt in die DHCP-Leases und announced die
Zahl via MQTT. Wegen einer noch nicht erfolgten Migration ist derzeit das Voice
im IRC noch kaputt.

Klingelevents finden sich jetzt auch im MQTT.

[APU]: https://stratum0.org/wiki/Strapu


Vorträge
--------

Mit 6 Vorträgen und mindestens 27 Entitäten im Chillraum zählten die
Novembervorträge zu den umfangreicheren:

* Emantor: Wireguard: Was? Wieso? Wie komme Ich da ran?
* Lulu: Delayed Halloween Special: Prepping für die Zombie-Apokalypse. Part 1: "Gravity Light"
* Daniel Bohrer: Plain Text Accounting: Buchhaltung mit ledger
* NoikK: 3D-Drucken im Stratum0
* blinry: Stammzellspende aus Spendersicht
* Drahflow: New shiny Vim things.

Aufzeichnungen von dreien der Vorträge sind auf [Youtube] zu finden.

Die Dezembervorträge waren mit 3 Vorträgen etwas knapper, aber auch diese
dürften sich bald auf YouTube finden lassen.

Hast du eine Vortragsidee? Trag dich jetzt schon für [Januar] ein!

[Youtube]: https://www.youtube.com/watch?v=FWOKHq6Y6Z4&list=PL0BgSfKC1V3UuQ4qvnHxdQHbh5JyC5f8C
[Januar]: https://stratum0.org/wiki/Vortr%C3%A4ge#Talks_am_Sonntag.2C_14._Januar_2018.2C_ab_19:00


Weltherrschaft
--------------

Siehe [Wikidata].

[Wikidata]: https://query.wikidata.org/#SELECT%20%3FitemLabel%0AWHERE%0A%7B%0A%20%3Fitem%20wdt%3AP3712%20wd%3AQ540229%20.%0A%09SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22[AUTO_LANGUAGE]%2Cen%22%20%7D%0A%7D


getDigital-Resteverwertung
--------------------------

Wir haben uns mal wieder für die getDigital-[Resteverwertung] angemeldet,
zuletzt hatten wir 2012 schonmal ein Paket bekommen. Die Sendung kam schneller
als erwartet und neben einigem crap waren auch wieder ein paar [coole Dinge]
dabei. 3 weitere, leicht zu reparierende Matheuhren (jetzt auch in Frickelraum
und Werkstatt), ein Asus-Router, bei dem kein 2,4Ghz WLAN geht (ist jetzt LEDE
drauf, hat 5×1GBit-Ports, liegt in der Netzwerkfookiste). Ein QiCharger, 3
powered USB-Hubs, großer Like-Button (wurde aus Gründen irgendwo versteckt),
USB-Yoda, Uselessbox, shitload of binäre Armbanduhren, Solarpowerbank,
Hintergrundgeräuschgenerator, Glasscheibenlautsprecher etc.

[Resteverwertung]: https://www.getdigital.de/community/resteverwertung.html
[coole Dinge]: https://twitter.com/Stratum0/status/935605177494712322


Misc
----

* Membercount: 100 \o/
* Spenden: Die quuxLogic Solutions GmbH hat uns 1000€ gespendet.
* Die Adapterbox verfügt jetzt über zwei USB-C auf USB-C/USB3/HDMI-Adapter Melopow M002.
* [stratum0.net] kann jetzt wieder v6.
* [PrivateBin] ist jetzt die neueste Version.

[stratum0.net]: https://stratum0.net
[PrivateBin]: https://paste.stratum0.org/


Termine
-------

*  Mo, 18.12. 18:00: Malkränzchen, offener Kreativabend
*  Mo, 18.12. 19:00: 34c3 Planungs- und Basteltreffen
*  Di, 19.12. 17:30 - 20:00: 3D-Druck(er) Sprechstunde
*  Di, 19.12. 19:00: 34c3 Planungs- und Basteltreffen
*  Mi, 20.12. 19:00: Freifunk-Treffen
*  Do, 21.12. 19:00 - 23:00: Captain's Log
*  Mo, 25.12. 18:00: Malkränzchen, offener Kreativabend
*  Di, 26.12. 18:00: Coptertreffen, Kopter Meeting im Stratum0
*  Mi, 27.12. bis Sa, 30.12.: 34c3, Leipzig
*  Do, 28.12. 18:30: Digital Courage Braunschweig, offenes Ortsgruppentreffen
*  Mo, 01.01. 18:00: Malkränzchen, offener Kreativabend
*  Di, 09.01. 18:00: Coptertreffen, Kopter Meeting im Stratum0
*  Do, 11.01. 18:30: Digital Courage Braunschweig, offenes Ortsgruppentreffen
*  Sa, 13.01. 14:30: CoderDojo Vorbereitungsbasteln
*  So, 14.01. 14:00: Mitgliederversammlung 2018-01-14
*  So, 14.01. 19:00: Vorträge


Willst du diesen Newsletter mitgestalten? Dann schicke deinen kurzen Beitrag an
<kontakt@stratum0.org>.

Happy Hacking!

