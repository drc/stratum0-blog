---
layout: post
title: "Stratumnews Vol. 20"
author: Stratumnews Team
date: 2017-07-01 21:00
comments: true
categories: [german,stratumnews]
---

Hallo Entitäten,

an dieser Stelle wieder eine zwei(±n)wöchentliche Zusammenfassung for all things space:
<!-- more -->

Infodisplay
-----------
Die SD-Karte vom Infodisplay-Pi ist gestorben. Auf einer neuen Karte wurde das System neu aufgesetzt, dabei wurde die [Anleitung im Wiki][iw] generalüberholt und sollte jetzt bei strikter Befolgung zu einem funktionierenden Infodisplay führen.

[iw]: https://stratum0.org/wiki/Infodisplay


Wurstworkshop
-------------
Der Wurstworkshop war wieder sehr erfolgreich, es sind nur noch einige Reste R5 im Gefrierschrank käuflich verfügbar, siehe [Pad][wp].
Es ist geplant, zeitnah noch einen weiteren Batch nachzuproduzieren, da die Nachfrage nach wie vor größer ist als das Angebot.

[wp]: https://pad.stratum0.org/p/wurst20170513


Enno2017
--------
Es wird wieder eine Eskalation geben! Diesmal die #enno2017.
Stattfinden wird die Feierlichkeit am 14.07. ab 13:37:42, Tickets gibt es im [Vorverkauf][ev]. Diese sind kostenfrei und nur dafür da, dass wir einen besseren Überblick haben, wie viele Leute kommen und wieviel Essen/Tschunk vorbereitet werden soll. Spacemember können mit dem Vouchercode (Stratum0-WLAN-Passwort) ein Ticket klicken. Dies ist dann auch noch möglich, nachdem das Kontigent überschritten ist.

[ev]: https://tickets.eskalation.rocks/TuTEskalation/enno2017/


3D-Drucker-Status
-----------------
Nach einigen Reparaturen funktionieren derzeit wieder alle drei 3D-Drucker. \o/


Lötecke
-------
Die Arbeitsplatzbeleuchtung wurde um viel LED erweitert. Auch die schwenkbare Arbeitsleuchte hat nach einem Defekt ein LED-Upgrade von Doom bekommen. \o/


Termine
-------

*  So, 02.07. 18:00: OSM Stammtisch Braunschweig
*  Mi, 05.07. 16:00: Schreibrunde
*  Mi, 05.07. 19:00: Freifunk-Treffen
*  Mi, 05.07. 19:00: Vegan Academy
*  Do, 06.07. 18:30 - 21:00: BS LUG
*  Do, 06.07. 19:00 - 23:00: Captain's Log
*  Mo, 10.07. 19:00: HOA: Orgatreffen
*  Di, 11.07. 18:00: Coptertreffen, Allgemeiner Infoaustausch
*  Mi, 12.07. 16:00: Schreibrunde
*  Mi, 12.07. 19:00: Freifunk-Treffen
*  Do, 13.07. 18:30 - 21:00: BS LUG
*  Do, 13.07. 19:00 - 23:00: Anime Referat
*  Fr, 14.07. 13:37 bis Sa, 15.07. 18:00:  #enno2017
*  Fr, 14.07. 19:00: Vorträge
*  Sa, 15.07. 15:00:  #enno2017-Aufräumtreff
*  Mi, 19.07. 16:00: Schreibrunde
*  Mi, 19.07. 19:00: Vegan Academy
*  Do, 20.07. 19:00 - 23:00: Captain's Log
*  Di, 25.07. 18:00: Coptertreffen, Allgemeiner Infoaustausch
*  Fr, 04.08. bis Di, 08.08.: SHA2017, Zeewolde (NL)
*  Sa, 19.08. 14:00: CoderDojo
*  Fr, 25.08. bis So, 27.08.: Maker Faire Hannover
*  Do, 07.09. bis So, 10.09.: HackenOpenAir
*  Fr, 15.09. bis So, 17.09.: LHC VI: Large Hackerspace Convention, Shackspace

Willst du diesen Newsletter mitgestalten? Dann schicke deinen kurzen Beitrag an <kontakt@stratum0.org>.

Happy Hacking!
