---
layout: post
title: "Stratumnews Vol. 27"
author: Stratumnews-Team
date: 2018-07-01 23:42
comments: true
categories: [german,stratumnews]
---

Hallo Entitäten,

an dieser Stelle wieder eine zwei(±n)wöchentliche Zusammenfassung for all things space:

<!-- more -->


Hacken Open Air 2018
--------------------

Es gibt auch dieses Jahr wieder ein Hacken Open Air, diesesmal Anfang August und auf dem Campingplatz Glockenheide.
Wer sich der Orga noch anschließen möchte, subscribed sich auf der [Mailingliste][] und kommt zu den nächsten Treffen.
Direktlink zum [Presale][], weitere Infos auf der [HOA][]-Website.

[Presale]: https://tickets.hackenopenair.de/hoa/2018/
[HOA]: https://hackenopenair.de/
[Mailingliste]: https://lists.rohieb.name/mailman/listinfo/hoa-orga


Kammer des Schreckens
---------------------

Die Kammer des Schreckens begleitet den Space seit Anbeginn, hier geht es um kritische Infrastruktur wie die Schließanlage und Türöffner.
Leider hat hier über die Jahre ein jeder Magier des Stratum 0 ein Rätsel hinterlassen. Eine Überarbeitung des ganzen Setupups ist geplant, es wurde sogar bereits Hardware beschafft, Platinen designed und geordert. Es müsste nur mal jemand™ einee Platine bestücken und das neue System in Betrieb nehmen. Neuerungen würde unter anderem auch eine Unterscheidung zwischen der Haustürklingel und der Spaceklingel umfassen.


Wurstworkshop
-------------

Wir haben wieder gewurstet, diesmal ca. 85 kg.
Gelernte Lektionen: Nächstes Mal werden wir 1-2 Wochen vor dem Workshop einen Rezeptworkshop machen und Rezepte in Kleinmengen durchprobieren. Keine Experimente am Workshoptag und alle Zutaten können zuverlässig vorher besorgt werden. Ebenso wollen wir einen Wurstequipmentmodifizierungsworkshop abhalten, um vorhandenes Equipment zu verbessern, insbesondere für bessere Kühlung.
Es ist noch Wurst da, alle Wurstpakete im Gefrierschrank, die nicht mit Namen versehen sind, stehen zum Verkauf, 11€/kg. Geld in die physische Wurst-Kasse auf dem Gefrierschrank.


Trokn
-----

Das bisherige "NAS" (Eigentlich nur ein Dingsi im Netzwerk mit ner mittelgroßen Platte dran) [NAStratum][] kam so langsam an seine Grenzen. Es wurde neue Hardware zur Verfügung gestellt, sodass nun auch mehr Platten angeschlossen werden können. NAStratum ist tot, lang lebe [Trokn][]. Für neue/größere Platten läuft gerade ein [Crowdfunding][] (siehe Mailingliste) und eine Suchaktion (nehmen alles ≥2TB).

[NAStratum]: https://stratum0.org/wiki/NAStratum
[Trokn]: https://stratum0.org/wiki/Trokn
[Crowdfunding]: https://stratum0.org/wiki/Crowdfunding


MakerFaire 2018
---------------

So wie es aussieht, werden wir wieder auf der MakerFaire in Hannover vertreten sein, wenn du vor Ort mit dabei sein möchtest, trag dich bitte ins [Pad][] mit ein, es wird voraussichtlich im Juli ein kleines Treffen dazu geben.

[Pad]: https://pad.stratum0.org/p/makerfaire2018


Space3
------

Um die Suche nach Erweiterungsmöglichkeiten/potentiellen neuen Spaces mal etwas besser zu koordinieren gibt es nun für alle Interessierten einen [Matrix-room][] (auch erreichbar über freenode: #space3).

[Matrix-room]: https://chat.stratum0.org/#/room/#space3:stratum0.org


Kurzmeldungen
-------------

* Für die Standbohrmaschine gibt es nun einen [Maschinenschraubstock][], einen Röhm DPV 3-W.
* [WLAN][] im Space hat sich geändert. Zunächst mit Unifi APs von Freifunk getestet haben wir jetzt 3 Unifi UAP AC Lite und einen UAP AC Pro angeschafft. Auf der [Strapu][] läuft jetzt zudem eine Unifi-Controller-VM.
* der RTFM-Ordner für Festool-Anleitungen ist ins Werkstatt-Regal gewandert.
* fnordcredit ist auf die APU [strapu][] umgezogen, sodass es von den Matekassen-Digital-Konten jetzt auch Backups gibt.
* Wir unterstützung jetzt offiziell den Offenen Brief ["Public Money, Public Code"][publiccode] der FSFE.
* Das [Freifunk-Event-WLAN][ffev] hat die erste Testphase erfolgreich abgeschlossen.
* Repetier-Server wurde auf Version 90 geupdated
* Im Oszilloskop in der Lötecke ist $etwas abgeraucht, mutmaßlich eine Komponente in der Kaltgerätebuchse vom Gerät selbst. Im Zuge der Reparatur wurde die Batterieeinheit unter dem Gerät entkernt, sodass das Oszi jetzt am Gerät selbst und nicht an der Batterieeinheit an- und auszuschalten ist.

[Maschinenschraubstock]: http://eshop.roehm.biz/roehm_de_de/dpv-3-w-mit-prismen-und-normalbacke-sbo.html
[WLAN]: https://twitter.com/DooMMasteR/status/979992079597211648
[strapu]: https://stratum0.org/wiki/Strapu
[publiccode]: https://publiccode.eu/openletter/
[ffev]: https://publiccode.eu/openletter/

Termine
-------

* Mo, 02.07. 18:00: Malkränzchen, offener Kreativabend
* Di, 03.07. 20:00 - 23:00: Anime-Referat
* Mi, 04.07. 19:00: Freifunk-Treffen
* Do, 05.07. 18:30: Home Automation
* Mo, 09.07. 18:00: Malkränzchen, offener Kreativabend
* Mo, 09.07. 19:00: HackenOpenAir 2018, Orgatreffen
* Di, 10.07. 18:00: Coptertreffen, Kopter Meeting im Stratum0
* Mi, 11.07. 19:00: Freifunk-Treffen
* Do, 12.07. 18:30: Digitalcourage Braunschweig, offenes Ortsgruppentreffen
* Sa, 14.07. 19:00: Vorträge (Aufzeichnungen)
* Mo, 16.07. 18:00: Malkränzchen, offener Kreativabend
* Di, 17.07. 19:00: HackenOpenAir 2018, Orgatreffen
* Di, 17.07. 20:00 - 23:00: Anime-Referat
* Mi, 18.07. 19:00: Freifunk-Treffen
* Do, 19.07. 18:30: Home Automation
* Do, 19.07. 19:00 - 23:00: Captain's Log
* Sa, 21.07. 14:00: CoderDojo
* Di, 24.07. 18:00: Coptertreffen, Kopter Meeting im Stratum0
* Mi, 25.07. 19:00: HackenOpenAir 2018, Orgatreffen
* Do, 26.07. 18:30: Digitalcourage Braunschweig, offenes Ortsgruppentreffen
* Do, 02.08. bis So, 05.08.: HackenOpenAir 2018
* Fr, 14.09. bis So, 16.09.: Maker Faire Hannover
* So, 23.09. 10:00 - 16:00: Kassenzwischenprüfung (Frickelraum reserviert)



Willst du diesen Newsletter mitgestalten? Dann schicke deinen kurzen Beitrag an <kontakt@stratum0.org>.

Happy Hacking!


