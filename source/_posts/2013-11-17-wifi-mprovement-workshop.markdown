---
layout: post
title: "WiFi-Improvement-Workshop"
date: 2013-11-17 21:47
author: DooMMasteR
comments: true
categories: [german,workshop,event]
---

Ohren auf, WLAN an, es wird in nächster Zeit einen Workshop geben und dazu eine Einführung im Rahmen der kommenden ($DATUM) Lightning-Talks.

Der Workshop richtet sich an viele von euch, alle Menschen die mit ihrer WLAN-Hardware im Notebook/Netbook unzufrieden sind.
Das Ziel wird es sein, das problematische WLAN-Modul durch ein ath9k basierendes auszutauschen, damit sind folgende Vorteile erreichbar:

* problemfreier Linux-Betrieb dank GPL-Treiber und Firmware (tagelang stabile und schnelle Verbindung)
* 803.11 a/b/g/n Support, also hohe Datenraten und freie Frequenzen, was z.B. auf dem 30C3 sehr angenehm sein kann
* Bluetooth 4.0 Support, sofern der mini-PCIe-Slot auch mit USB belegt ist
* geringer Stromverbrauch dank funktionierendem Powermanagement

In der Praxis gibt es mit dem ath9k-Treiber unter Linux quasi keine Betriebsprobleme mehr, eine 2x2 Karte erreicht ohne weiteres ~180MBit/s (netto) und es funktioniert alles was man sich so vorstellen kann (Monitor-Mode, AP-Mode per hostapd usw.).
Leider gibt aber ggf. einige Probleme zu umschiffen, vorallem bei Lenovo Notebooks, denn manche Hersteller verhindern mit Whitelisting initial die Verwendung eines herstellerfremden WLAN-Moduls.

Ach ja, ein Punkt noch, die Kosten: variieren, aber liegen beim AR5B22-Modul bei ~12€ inkl. Versandkosten, andere ath9k-Module sind meist etwas teurer (es gibt auch 3x3-Karten für alle Speedjunkies) und schlimmstenfalls sind noch 2 Pigtails nötig, mehr dazu und zu allem anderen dann beim Talk.
